require 'zip'
module ImportModel
  ##
  # Export::Project digunakan untuk memproses data mobil
  # dari CSV atau Excel yang diterima dari parameter
  # serta mengolah gambar mobil yang filenya diikut sertakan dalam parameter.
  ##
  class Project
    extend ActiveSupport::Concern
    attr_accessor :file, :columns, :spreadsheet, :errors, :start_row
    attr_accessor :current_row, :current_row_data, :valid_records
    attr_accessor :invalid_records, :car_logos, :file_image, :folder_image

    class << self
      ##
      # unzip_folder:
      #   method yang digunakan untuk mengektrak file archive
      #   yang diterima dari parameter ke folder yang dituju
      #   dan jika foldernya tidak ada maka akan dibuat baru
      ##
      def unzip_folder (file, destination)
        Zip::File.open(file) { |zip_file|
         zip_file.each { |f|
           f_path=File.join(destination, f.name)
           FileUtils.mkdir_p(File.dirname(f_path))
           zip_file.extract(f, f_path) unless File.exist?(f_path)
         }
        }
      end

      ##
      # import_columns:
      #   method yang digunakan sebagai default header dan column position.
      #   method ini akan digunakan pada halaman export untuk memberitahu user tentang
      #   column apa saja yang tersedia serta posisinya pada excel atau csv
      ##
      def import_columns
        {
          code: 0,
          title: 1,
          description: 2,
          price: 3,
          activated: 4,
          featured: 5,
          user_email: 6,
          type_project: 7,
          project_size: 8,
          construction: 9,
          possession_start: 10,
          insights: 11,
          facilities: 12,
          address: 13,
          city: 14,
          province: 15,
          postcode: 16,
          latitude: 17,
          longitude: 18,
          sub_district: 19,
          village: 20,
          area: 21,
          overview_title: 22,
          overview_possession_start: 23,
          overview_saleable_area: 24,
          overview_price: 25,
          overview_position: 26,
          overview_toilets: 27,
          overview_balconies: 28,
          interior_title: 29,
          interior_val: 30,
          interior_position: 31,
          interior_category: 32,
          interior_title_1: 33,
          interior_val_1: 34,
          interior_position_1: 35,
          interior_category_1: 36,
          interior_title_2: 37,
          interior_val_2: 38,
          interior_position_2: 39,
          interior_category_2: 40
        }
      end

    end

    attr_accessor :file, :file_image, :listing_type, :start_row
    def initialize(args={})
      args.each do |args_name, args_value|
        self.send("#{args_name}=", args_value)
      end
      @columns, @valid_records, @invalid_records, @user_cars = {}, {}, {}, {}

      folder_name = rand(Time.now.to_i)*rand(100)
      @folder_image = "#{Rails.root}/tmp/#{folder_name}"

      args[:columns].each do |k, v|
        next unless !k.is_a?(String) || !k.is_a?(Symbol)
        next if k.blank? || v.blank?
        @columns[k.to_sym] = v.to_i
      end if args[:columns].present?
    end



    def start
      @file                = File.new(copy_upload_folder(@file))
      @file_image          = File.new(copy_upload_folder(@file_image)) if @file_image.present?

      return complete_process unless is_valid?
      if @file_image && File.exist?(@file_image.path)
        ImportModel::Project.unzip_folder(@file_image.path, @folder_image)
      end

      @spreadsheet = open_spreadsheet
      @current_row = @start_row.to_i < 2 ? 2 : @start_row.to_i

      while @current_row <= @spreadsheet.try(:last_row).to_i do
        @current_row_data = @spreadsheet.row(@current_row)
        if valid_row?
          options = get_project_params
          options[:facilities] = options[:facilities].split(', ') if options[:facilities].present?
          if options[:user_email].blank?
            project = ::Project.new
            project.errors.add(:user, "is not exist with object project #{options}")
            add_errors(project, 'Project')
          else
            options.delete(:user_email)
            project = ::Project.new(options)
            if project.save
              @valid_records[@current_row] = {row: @current_row_data}
              # upload_images(project)
            else
              add_errors(project,'Project')
            end
          end
        end

        @current_row += 1
      end
      complete_process
    end

    def copy_upload_folder(file)
      new_filename = [Time.now.to_i, File.extname(file.original_filename)].join('')
      path_to_file = "#{Rails.root}/tmp/#{new_filename}"
      FileUtils.cp(file.path, path_to_file)
      return path_to_file
    end

    def complete_process
      if @file && File.exist?(@file.path)
        FileUtils.rm(@file.path)
      end
      # ExportMailer.report(self).deliver
      # web_setting = WebSetting.first || WebSetting.new
      # @user_cars.each do |user_id, user_data|
      #   _cars = ::Project.find(user_data[:cars])
      #   user = ::User.find(user_id)

      #   # user.send :generate_confirmation_token! if !user.confirmed?
      #   # if (['b','b2'].include? @listing_type.to_s.downcase) && user.email.present?
      #   #   ExportMailer.email_dealer(user, _cars, user_data[:password], @listing_type, web_setting).deliver
      #   # elsif @listing_type.to_s.downcase == 'auction' && user.email.present?
      #   #   ExportMailer.email_auction(user, _cars, user_data[:password]).deliver
      #   # end
      # end
      FileUtils.rm_rf(@folder_image)
      FileUtils.rm(@file_image.path) rescue nil if @file_image.present?
    end

    def generate_username(email)
      @username = email.split("@").first
      available_username = User.where("lower(username) = ?", @username.downcase)
      if available_username
        @username = @username + rand(1000).to_s
      end
    end

    def valid_row?
      row_errors = []
      row_errors.push('Title is blank') if get_value(:title).blank?
      row_errors.push('Not User Present') if get_value(:user_email).blank?
      return true if row_errors.size == 0
      @invalid_records[@current_row] ||= {row: @current_row_data, errors: []}
      @invalid_records[@current_row][:errors].push(row_errors)
      @invalid_records[@current_row][:errors] = @invalid_records[@current_row][:errors].flatten.uniq
      return false
    end

    def is_valid?
      @errors ||= []
      @errors.push('File is not exist') if @file.blank?
      @errors.push('Header columns can not be defined') if @columns.blank?
      @errors.push('Start row is not integer or defined') if @start_row.blank?

      @errors.blank?
    end

    def add_errors(object, type)
      @invalid_records[@current_row] ||= {row: @current_row_data, errors: []}
      @invalid_records[@current_row][:errors].push(object.errors.full_messages)
      @invalid_records[@current_row][:errors].push("Error on #{type}")
      @invalid_records[@current_row][:errors] = @invalid_records[@current_row][:errors].flatten.uniq
      false
    end

    def get_user_id(email)
      user = User.find_or_initialize_by(email: email)
      if user.new_record?
        user.username = generate_username(email)
        user.password = 12345678
        user.password_confirmation = 12345678
        user.save
      end
      user.id
    end

    def get_region_id(region_name, parent_id=nil)
      if parent_id.present?
        parent = Region.find(parent_id)
        region = parent.children.find_or_initialize_by(name: region_name)
      else
        region = Region.find_or_initialize_by(name: region_name)
      end
      if region.new_record?
        region.parent_id = parent_id
        region.save
      end
      region.id
    end

    def get_project_params
      province_id = get_region_id(get_value(:province))
      city_id = get_region_id(get_value(:city), province_id)
      sub_district_id = get_region_id(get_value(:sub_district), city_id)
      village_id = get_region_id(get_value(:village), sub_district_id)
      area_id = get_region_id(get_value(:area), village_id)
      activated = get_value(:activated).present? ? get_value(:activated) : true
      {
        code: get_value(:code),
        title: get_value(:title),
        description: get_value(:description),
        price: get_value(:price),
        status: get_value(:status),
        activated: activated,
        featured: get_value(:featured),
        user_email: get_value(:user_email),
        user_id: get_user_id(get_value(:user_email)),
        type_project: get_value(:type_project),
        project_size: get_value(:project_size),
        construction: get_value(:construction),
        started_at: Date.today,
        expired_at: Date.today + 30.days,
        possession_start: get_value(:possession_start),
        insights: get_value(:insights),
        facilities: get_value(:facilities),
        address_attributes: {
          address: get_value(:address),
          province_id: province_id,
          city_id: city_id,
          subdistrict_id: sub_district_id,
          village_id: village_id,
          area_id: area_id,
          postcode: get_value(:postcode),
          latitude: get_value(:latitude),
          longitude: get_value(:longitude),
        },
        overviews_attributes: { '0': {
          title: get_value(:overview_title),
          possession_start: get_value(:overview_possession_start),
          saleable_area: get_value(:overview_saleable_area),
          price: get_value(:overview_price),
          position: get_value(:overview_position).to_i,
          toilets: get_value(:overview_toilets),
          balconies: get_value(:overview_balconies),
          interiors_attributes: { '0': {
              title: get_value(:interior_title),
              val: get_value(:interior_val),
              position: get_value(:interior_position).to_i,
              category: get_value(:interior_category)
              },
              '1': {
                title: get_value(:interior_title_1),
                val: get_value(:interior_val_1),
                position: get_value(:interior_position_1).to_i,
                category: get_value(:interior_category_1)
              },
              '2': {
                title: get_value(:interior_title_2),
                val: get_value(:interior_val_2),
                position: get_value(:interior_position_2).to_i,
                category: get_value(:interior_category_2)
              },
            }
          },
        }
      }
    end

    def get_value(attr)
      return '' if @columns[attr].blank?
      col_index = @columns[attr].to_i - 1
      @current_row_data[col_index].to_s.clean_space.gsub(/(\.0$)|(^\-$)/,'')
    end

    def open_spreadsheet
      case extname_file
        # when '.csv'  then Roo::CSV.new(@file.tempfile.path)
        # when '.xls'  then Roo::Excel.new(@file.tempfile.path, :nil, :ignore)
        # when '.xlsx' then Roo::Excelx.new(@file.tempfile.path, :nil, :ignore)
        when '.csv'  then Roo::CSV.new(@file.path)
        when '.xls'  then Roo::Excel.new(@file.path, :nil, :ignore) rescue false
        when '.xlsx' then Roo::Excelx.new(@file.path, :nil, :ignore)
      else
        raise "Unknown File Type #{file.filename}"
      end
    end

    def extname_file
      # File.extname(@file.filename)
      File.extname(@file.path)
    end

  end
end
