module Permitable
	USER = [
    :id,
    :type,
    :username,
    :email,
    :password,
    :password_confirmation,
    :provider,
    :uid,
    :oauth_token,
    :oauth_expires_at,
    :slug,
    :verified,
    :code,
    :role_id
  ]

  PROFILE = {
    profile_attributes: [
      :id,
      :user_id,
      :full_name,
      :birthday,
      :gender,
      :avatar,
      :cover,
      :meta_title,
      :meta_keyword,
      :meta_description
    ]
  }

  CONTACT = {
    contact_attributes: [
      :id,
      :phone,
      :handphone,
      :pin,
      :facebook,
      :twitter,
      :contactable_type,
      :contactable_id,
      :contactable,
      :website
    ]
  }

  ADDRESS = {
    address_attributes: [
      :id,
      :address,
      :sub_district,
      :city,
      :province,
      :postcode,
      :addressable_type,
      :addressable_id,
      :addressable,
      :latitude,
      :longitude,
      :province_id,
      :city_id,
      :subdistrict_id,
      :village_id,
      :area_id,
      :area_name
    ]
  }

  AGENT_INFO = {
    agent_info_attributes: [
      :id,
      :user_id,
      :name_company,
      :company_logo,
      :company_web,
      :specialist_property,
      :specialist_area,
      :description,
      :language
    ]
  }

  REGION = [
    :id,
    :name,
    :ancestry,
    :featured,
    :parent_id,
    :image
  ]

  PROPERTY = [
    :id,
    :code,
    :slug,
    :title,
    :description,
    :price,
    :status,
    :activated,
    :featured,
    :user_id,
    :type_property,
    :category,
    :negotiable,
    :price_type,
    :started_at,
    :expired_at,
    :facilities,
    :sold_out,
    :label_tag
  ]

  PROJECT = [
    :id,
    :code,
    :slug,
    :title,
    :description,
    :price,
    :status,
    :activated,
    :featured,
    :user_id,
    :type_project,
    :project_size,
    :construction,
    :insights,
    :possession_start,
    :started_at,
    :expired_at,
    :facilities,
    :area
  ]

  OVERVIEW = {
    overviews_attributes: [
      :id,
      :title,
      :file_2d,
      :file_3d,
      :possession_start,
      :saleable_area,
      :price,
      :overviewable_type,
      :overviewable_id,
      :overviewable,
      :position,
      :toilets,
      :balconies,
      :_destroy,
      interiors_attributes: [
        :id,
        :title,
        :val,
        :position,
        :interiorable_type,
        :interiorable_id,
        :interiorable,
        :category,
        :_destroy
      ],
      amenities_attributes: [
        :id,
        :title,
        :available,
        :amenitiable_type,
        :amenitiable_id,
        :amenitiable,
        :icon,
        :position,
        :_destroy
      ]
    ]
  }

  INTERIOR = {
    interiors_attributes: [
      :id,
      :title,
      :val,
      :position,
      :interiorable_type,
      :interiorable_id,
      :interiorable,
      :category
    ]
  }

  BASIC_PROPERTY = {
    basic_property_attributes: [
      :id,
      :property_id,
      :bathrooms,
      :bedrooms,
      :parking,
      :facing,
      :floor,
      :age,
      :built_up_area,
      :document,
      :surface_area,
      :building_area,
      :electricity,
      :phone_lines,
      :garage,
      :price_in_meter,
      :business_unit,
      :width_land,
      :long_land,
      :price_security,
      :price_day,
      :price_month,
      :price_year
    ]
  }

  PROPERTY_TYPE = [
    :name,
    :parent_id,
    :ancestry,
    :slug
  ]

  RENT_PRICE = {
    rent_prices_attributes: [
      :name,
      :price,
      :price_type,
      :rent_priceable,
      :rent_priceable_type,
      :rent_priceable_id
    ]
  }

  SOCIETY = {
    societies_attributes: [
      :id,
      :title,
      :available,
      :societiable_type,
      :societiable_id,
      :societiable,
      :icon,
      :position,
      :_destroy
    ]
  }

  AMENITY = {
    amenities_attributes: [
      :id,
      :title,
      :available,
      :amenitiable_type,
      :amenitiable_id,
      :amenitiable,
      :icon,
      :position,
      :_destroy
    ]
  }

  ASSET_FACILITY = [
    :name,
    :font_icon,
    :category
  ]

  AUCTION = {
    auction_attributes: [
      :id,
      :property_id,
      :start_auction,
      :end_auction,
      :reserve_met,
      :financing_considered,
      :minimum_price,
      :increment_per_bid,
      :starting_bid
    ]
  }

  GALLERY = {
    galleries_attributes: [
      :id,
      :title,
      :description,
      :file,
      :galleriable_type,
      :galleriable_id,
      :galleriable,
      :position,
      :_destroy
    ]
  }

  LOCATION = [
    :id,
    :title,
    :latitude,
    :longitude,
    :user_id,
    :type_asset,
    :verified
  ]

  WEB_SETTING = [
    :id,
    :header_tags,
    :footer_tags,
    :contact,
    :email,
    :favicon,
    :logo,
    :facebook,
    :twitter,
    :title,
    :keywords,
    :description,
    :robot,
    :author,
    :corpyright,
    :revisit,
    :expires,
    :revisit_after,
    :geo_placename,
    :language,
    :country,
    :content_language,
    :distribution,
    :generator,
    :rating,
    :target,
    :search_engines,
    :package_title,
    :package_description
  ]

  LANDING_PAGE = [
    :title,
    :slug,
    :description,
    :status,
    :category,
    :link_url
  ]

  WISHLIST = [
    :user_id,
    :property_id
  ]

  INQUIRY = [
    :user_id,
    :name,
    :email,
    :phone,
    :message,
    :inquiriable_type,
    :inquiriable_id,
    :inquiriable,
    :status,
    :ancestry,
    :parent_id,
    :owner_id,
    :subject
  ]

  FEEDBACK = [
    :id,
    :name,
    :email,
    :category,
    :message,
    :phone,
    :status,
    :code
  ]

  REPORT = [
    :id,
    :name,
    :email,
    :category,
    :message,
    :reportable,
    :reportable_id,
    :reportable_type,
    :status,
    :code
  ]

  SCHEDULE_VISIT = [
    :id,
    :first_name,
    :last_name,
    :schedule_date,
    :alt_schedule_date,
    :phone,
    :email,
    :schedule_time,
    :alt_schedule_time,
    :property_id
  ]

  SOLD_REPORT = [
    :id,
    :user_id,
    :price,
    :sold_reportable,
    :sold_reportable_type,
    :sold_reportable_id,
    :date
  ]

  SUPPORTED = [
    :id,
    :title,
    :logo,
    :link
  ]

  TESTIMONIAL = [
    :id,
    :name,
    :email,
    :position,
    :user_id,
    :message,
    :activated,
    :name_company
  ]

  ROLE = [
    :id,
    :name,
    :title,
    :description,
    :the_role,
  ]

  PACKAGE = [
    :id,
    :name,
    :duration,
    :description,
    :price,
    :price_year,
    :active,
    :max_listing,
    :featured_listing,
    :top_listing,
    :gratis_bulan,
    :label_tags
  ]

  def self.controller(name)
    self.send name.gsub(/\W/,'_').singularize.downcase
  end

  # ROLE ADMIN ---------------------------------------------------

  def self.backend_user
    USER.dup.push(PROFILE.dup).push(ADDRESS.dup).push(CONTACT.dup)
  end

  def self.backend_admin
    backend_user
  end

  def self.backend_member
    backend_user.dup.push(AGENT_INFO.dup)
  end

  def self.backend_agent
    backend_user.dup.push(AGENT_INFO.dup)
  end

  def self.backend_developer
    backend_user
  end

  def self.backend_property
    PROPERTY.dup.push(GALLERY.dup).push(ADDRESS.dup).push(BASIC_PROPERTY.dup).push(AUCTION.dup)
  end

  def  self.backend_auction_property
    backend_property
  end

  def self.backend_property_type
    PROPERTY_TYPE
  end

  def self.backend_asset_facility
    ASSET_FACILITY
  end

  def self.backend_project
    PROJECT.dup.push(GALLERY.dup).push(ADDRESS.dup).push(SOCIETY.dup).push(OVERVIEW.dup)
  end

  def self.backend_region
    REGION
  end

  def self.backend_location
    LOCATION.dup.push(ADDRESS.dup)
  end

  def self.backend_landing_page
    LANDING_PAGE
  end

  def self.backend_web_setting
    WEB_SETTING.dup.push(GALLERY.dup)
  end

  def self.backend_supported
    SUPPORTED
  end

  def self.backend_package
    PACKAGE
  end

  def self.backend_testimonial
    TESTIMONIAL
  end

  def self.backend_feedback
    FEEDBACK
  end

  def self.backend_report
    REPORT
  end

  def self.backend_role
    ROLE
  end

  # ROLE MEMBER ---------------------------------------------------

  def self.userpage_member
    backend_member
  end

  def self.userpage_agent
    backend_agent
  end

  def self.userpage_property
    backend_property.dup.push(AUCTION.dup)
  end

  def self.userpage_wishlist
    WISHLIST
  end

  def self.userpage_inquiry
    INQUIRY
  end

  def self.userpage_sold_report
    SOLD_REPORT
  end

  # ROLE DEVELOPER -------------------------------------------------

  def self.developerpage_developer
    backend_user
  end

  def self.developerpage_project
    backend_project
  end

  def self.developerpage_wishlist
    WISHLIST
  end

  def self.developerpage_inquiry
    INQUIRY
  end

  # ROLE FRONT -----------------------------------------------------

  def self.property
    backend_property.dup.push(AUCTION.dup)
  end

  def self.wishlist
    WISHLIST
  end

  def self.inquiry
    INQUIRY
  end

  def self.feedback
    FEEDBACK
  end

  def self.report
    REPORT
  end

  def self.schedule_visit
    SCHEDULE_VISIT
  end

  def self.userpage_package
    PACKAGE
  end

end
