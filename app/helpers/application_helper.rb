module ApplicationHelper

  def find_facilities(type)
    AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{type}%')") rescue []
  end

  def checking_wishlist(id, current_user, url)
    checking = Wishlist.where(property_id: id, user_id: current_user)

    if checking.present?
      return '<i class="fa fa-heart set active"></i>'
    else
      return "<i class='fa fa-heart-o set click-wishlist' url='#{url}'></i>"
    end
  end

  def is_nav_list?
    params[:controller] == 'properties' && params[:action] == 'index' ||
    params[:controller] == 'projects' && params[:action] == 'index'
  end

  def user_edit_url(type, current_user)
    if type == 'Member'
      edit_userpage_member_path(current_user)
    elsif type == 'Agent'
      edit_userpage_agent_path(current_user)
    end
  end

  def show_notice_url(message_id)
    if current_user.developer?
      return developerpage_inquiry_path(message_id)
    else
      return userpage_inquiry_path(message_id)
    end
  end

  def notices_url?
    if current_user.developer?
      return developerpage_inquiries_path
    else
      return userpage_inquiries_path
    end
  end

	def number_to_currency_br(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end

  def number_to_currency_no_unit(number)
    number_to_currency(number, :unit => "", :separator => ",", :delimiter => ".", precision: 0)
  end

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
        concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
          concat content_tag(:span, 'Close', class: 'sr-only')
        end)
        if flash[:errors].present?
          message.each do |word|
            concat "<div class='alert alert-danger alert-dismissible'>#{word}</div>".html_safe
          end
        else
          concat message
        end
      end)
    end
    nil
  end

  def currency(price, precision = 0, unit=" ")
    number_to_currency(price, precision: precision, unit: unit)
  end

	def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'open'
    end if controller_name == controller.to_s
  end

  def active_sidebar_active?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def show_hidden(ancestry_depth, el)
    return "hidden" if ancestry_depth.blank?
    if el == "city"
      if ancestry_depth > 1
        return ""
      else
        return "hidden"
      end
    elsif el == "subdistrict"
      if ancestry_depth > 2
        return ""
      else
        return "hidden"
      end
    elsif el == "village"
      if ancestry_depth > 3
        return ""
      else
        return "hidden"
      end
    else
      return "hidden"
    end
  end

  def schedule_time
    @schedule_time = [
      ['7:00 AM','7:00 AM'],
      ['7:30 AM','7:30 AM'],
      ['8:00 AM','8:00 AM'],
      ['8:30 AM','8:30 AM'],
      ['9:00 AM','9:00 AM'],
      ['9:30 AM','9:30 AM'],
      ['10:00 AM','10:00 AM'],
      ['10:30 AM','10:30 AM'],
      ['11:00 AM','11:00 AM'],
      ['11:30 AM','11:30 AM'],
      ['12:00 PM','12:00 PM'],
      ['12:30 PM','12:30 PM'],
      ['1:00 PM','1:00 PM'],
      ['1:30 PM','1:30 PM'],
      ['2:00 PM','2:00 PM'],
      ['2:30 PM','2:30 PM'],
      ['3:00 PM','3:00 PM'],
      ['3:30 PM','3:30 PM'],
      ['4:00 PM','4:00 PM'],
      ['4:30 PM','4:30 PM'],
      ['5:00 PM','5:00 PM'],
      ['5:30 PM','5:30 PM'],
      ['6:00 PM','6:00 PM'],
      ['6:30 PM','6:30 PM'],
      ['7:00 PM','7:00 PM'],
      ['7:30 PM','7:30 PM'],
      ['8:00 PM','8:00 PM'],
      ['8:30 PM','8:30 PM'],
      ['9:00 PM','9:00 PM'],
      ['9:30 PM','9:30 PM'],
      ['10:00 PM','10:00 PM'],
      ['10:30 PM','10:30 PM'],
      ['11:00 PM','11:00 PM'],
      ['11:30 PM','11:30 PM'],
      ['12:00 AM','12:00 AM'],
      ['12:30 AM','12:30 AM'],
      ['1:00 AM','1:00 AM'],
      ['1:30 AM','1:30 AM'],
      ['2:00 AM','2:00 AM'],
      ['2:30 AM','2:30 AM'],
      ['3:00 AM','3:00 AM'],
      ['3:30 AM','3:30 AM'],
      ['4:00 AM','4:00 AM'],
      ['4:30 AM','4:30 AM'],
      ['5:00 AM','5:00 AM'],
      ['5:30 AM','5:30 AM'],
      ['6:00 AM','6:00 AM'],
      ['6:30 AM','6:30 AM']
    ]
  end

  def imdb_style_rating(rateable_obj, dimension, options = {})
    #TODO: add option to change the star icon
    overall_avg = rateable_obj.calculate_overall_average_dimension(dimension)

    content_tag :div, '', :style => "background-image:url('#{image_path('big-star.png')}');width:81px;height:81px;margin-top:10px;" do
        content_tag :p, overall_avg, :style => "position:relative;line-height:110px;text-align:center;"
    end
  end

  def rating_user(stars=0)
    html = ''
    split_star = stars.to_s.split('.')
    full_star = split_star.first.to_i
    half_star = split_star.last.to_i
    full_star.times do |i|
      html += "<img alt='#{i}' src='/assets/star-on.png'> &nbsp;"
    end
    html += "<img alt='half' src='/assets/star-half.png'> &nbsp;" if half_star != 0
    return html.html_safe
  end

  def show_subscribe(current_user, package_id)
    html = ''
    if current_user.package_payed?(package_id) && !current_user.package_expire?(package_id)
      html = link_to 'Already Subscribe', userpage_packages_path(package_id: package_id), method: 'post', class: 'btn btn-info btn-sm full-width', data: { confirm: 'Do You Agree To Buy This Package Again'}
    elsif current_user.package_not_payed?(package_id)
      html = "<div class='btn btn-info btn-sm full-width'>Wait Confirmation</div>"
    else
      html = link_to 'Order Now', userpage_packages_path(package_id: package_id), method: 'post', class: 'btn btn-info btn-sm full-width'
    end
    return html.html_safe
  end

  def downcase_uderscore(str)
    str.downcase.gsub(' ', '_')
  end

  def number_to_human_indo(number)
    number = number.gsub('.', '_').to_i
    number_to_human(number, units: {thousand: "Ribu", million: 'Juta', billion: 'Milyar'})
  end

end
