class UserMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper
  helper :application

	default from: 'creativedesignjakarta@gmail.com'
	default to: 'creativedesignjakarta@gmail.com'

	# USER MAILER

	def welcome_join(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Welcome join to PROPEDIA')
	end

	def send_request_user_to_agent(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(from: user.email, subject: 'Request Verification - upgrade account to agent')
	end

	def send_user_approve_to_agent(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Approved Verification - Your Account has change to agent')
	end

	def send_user_approve_to_member(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Approved Verification - Your Account Member has Verified')
	end

	def send_user_approve_to_developer(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Approved Verification - Your Account Developer has Verified')
	end

	# FEEDBACK MAILER

	def send_feedback(feedback)
		#@logo = "#{root_url}assets/logo.png"
		@feedback = feedback
		mail(from: feedback.email, subject: "Received Feedback - #{feedback.code}")
	end

	# REPORT MAILER

	def send_report(report)
		#@logo = "#{root_url}assets/logo.png"
		@report = report
		mail(from: report.email, subject: "Received Report - #{report.code}")
	end

	# PROPERTY MAILER

	def send_request_catalog(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(from: catalog.user.email, subject: "Request New Property to Publish - #{catalog.code}")
	end

	def send_catalog_verified(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Property Already Publish - #{catalog.code}")
	end

	def send_catalog_featured(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Set Property to Featured  - #{catalog.code}")
	end

	# PROJECT MAILER

	def send_request_project(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(from: catalog.user.email, subject: "Request New Project to Publish - #{catalog.code}")
	end

	def send_project_verified(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Project Already Publish - #{catalog.code}")
	end

	def send_project_featured(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Set Project to Featured  - #{catalog.code}")
	end

	def send_same_property(catalogs, user, village_name)
		@user = user
		@catalogs = catalogs
		@village_name = village_name
		mail(to: @user.email, subject: "New Property Same With Your Wishlist")
	end

end
