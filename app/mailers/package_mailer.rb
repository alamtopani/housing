class PackageMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper

  default from: 'creativedesignjakarta@gmail.com'
  default to: 'creativedesignjakarta@gmail.com'

  def buy_package(user_package)
    @url = confirmations_backend_order_user_package_path(user_package.id)
    @user = user_package.user
    @user_package = user_package
    mail(subject: "User Have Buy Package")
  end

  def invoice_package(user_package)
    @user = user_package.user
    @package = user_package.package
    @user_package = user_package
    @price_package = number_to_currency_br(@user_package.price)
    mail(to: @user.email, subject: "Invoice Package")
  end

  def confirmation(user_package)
    @url = confirmations_backend_order_user_package_path(user_package.id)
    @user = user_package.user
    @package = user_package.package
    @user_package = user_package
    @price_package = number_to_currency_br(@user_package.price)
    mail(to: @user.email, subject: "Info:: Package Has Actived")
  end

  def number_to_currency_br(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end


end
