class PropertiesController < FrontendController
  impressionist actions: [:show]
	before_filter :prepare_map_types, only: [:index, :show]
	before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_facilities_new, only: [:new]

	layout 'application_properties_map', only: [:index, :show]

	def index
    search = params[:region_display_name].split(',').first if params[:region_display_name]
    reg = search.present? ? search : params[:province_string].present? ? params[:province_string] : params[:province].present? ? params[:province] : "DKI Jakarta"
    params[:province_string] = params[:province_string].present? ? params[:province_string] : "DKI Jakarta" if params[:region_display_name].blank?
    @featureds = Property.search_by(params).activated.latest.featured.not_auction.limit(16)
    if params[:search].present?
      prepare_facilities_search(params[:type_property]) if params[:type_property].present?
    end

    @top_properties = Property.not_auction.activated.search_by(params).top_listing
    if params[:list].present?
      @region = Region.where(name: reg).first
      @list_properties = Property.not_auction.search_by(params).activated.latest
      render :map
    else
      if params[:category].blank?
        @properties = collection.not_auction
      elsif params[:category] == 'Auction Property'
        @properties = collection.activated_auction
      else
        @properties = collection
      end

      @properties_count = @properties.size

			@properties = @properties.page(params[:page]).per(params[:per_page])
      respond_to do |format|
  			format.html { render :list }
        format.js { render 'index.js'}
      end
		end
	end

  def featured
    @featureds = Property.search_by(params).activated.latest.featured.not_auction.page(params[:page]).per(params[:per_page])
  end

  def auction
    @properties = collection.activated_auction.limit(10)
  end

	def new
		@property = Property.new
    @packages = Package.actived.oldest
	end

	def create
		@property = Property.new(permitted_params)
		@property.user_id = current_user.id
    @property.facilities = params[:facilities]
		respond_to do |format|
      if @property.save
        UserMailer.send_request_catalog(@property).deliver
        format.html {redirect_to userpage_properties_path, notice: 'Property was successfully created'}
      else
        flash[:errors] = @property.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	def show
		@property = Property.find(params[:id])
		@inquiry = Inquiry.new
    @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{@property.type_property}%')")
    impressionist(@property, 'message')
	end

	def wishlist
    checking = Wishlist.where(property_id: params[:id], user_id: current_user.id)

    if checking.present?
      redirect_to :back, alert: "This property is already on your property wishlist!"
    else
      @wishlist             = Wishlist.new
      @wishlist.user_id     = current_user.id
      @wishlist.property_id = params[:id]

      if @wishlist.save
        redirect_to :back, notice: "Property was successfully add to wishlists!"
      else
        redirect_to :back, alert: "Property was not successfully add to wishlists!"
      end
    end
  end

  def checkout_bid
    @bid_auction = BidAuction.new
    @bid_auction.user_id = current_user.id
    @bid_auction.property_id = params[:property_id]
    @bid_auction.bid = params[:bid]

    @property = Property.find params[:property_id]
    if params[:bid].to_i < @property.the_price_bid?.to_i
      redirect_to :back, alert: "Your bid must be greater than the price of property or highest bid!"
    else
      if @bid_auction.save
        redirect_to :back, notice: "Your bid was successfully created!"
      else
        redirect_to :back, alert: "Your bid was not successfully created!"
      end
    end
  end

	private
    def collection
      @collection ||= Property.search_by(params).activated
                              .latest
    end

		def prepare_map_types
      @types   = PropertyType.roots.where.not(name: 'New Project').pluck(:name)
      @types_location = LocationType.to_a
      @facilities = AssetFacility.pluck(:name)
      @assets = []
    end

    def prepare_map_location
      @types   = PropertyType.roots.where.not(name: 'New Project').pluck(:name)
      @others = []
		end

		def permitted_params
			params.require(:property).permit(Permitable.controller(params[:controller]))
		end

    def prepare_facilities_new
      @facilities = AssetFacility.latest
    end

    def prepare_facilities_search(type)
      @facility_list = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{type}%')")
    end
end
