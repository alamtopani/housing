class Userpage::AgentsController < Userpage::ApplicationController
	defaults resource_class: Agent, collection_name: 'agents', instance_name: 'agent'
  load_and_authorize_resource
  
	add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Agents"

  def update
  	update! do |format|
  		if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
  	end
  end
end
