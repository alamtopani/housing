class Userpage::PackagesController < Userpage::ApplicationController  
  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Packages", :collection_path

  def index
    @current_package = current_user.current_package
  end

  def invoices
    @user_packages = current_user.user_packages.latest.page(params[:page]).per(params[:per_page])
  end

  def invoice
    @user_package = UserPackage.find_by(code: params[:id])
  end

  def create
    user_package = current_user.user_packages.new(package_id: params[:package_id])
    if user_package.save
      PackageMailer.buy_package(user_package).deliver
      PackageMailer.invoice_package(user_package).deliver
      redirect_to :back, notice: 'Package Already Ordered, Please Pay it And Wait Confirmation'
    else
      redirect_to :back, error: user_package.errors.full_messages
    end
  end

end
