class Userpage::SoldReportsController < Userpage::ApplicationController
  load_and_authorize_resource
  
  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Sales Reports", :collection_path

  def index
  	@reports = current_user.sold_reports.latest
    @collection = @reports.page(page).per(per_page)
  end

  def destroy
  	if resource.destroy
  		@property = resource.sold_reportable
  		@property.update(sold_out: false)
  		@property.save
  		redirect_to :back, notice: 'Property Report Successfully Deleted'
  	else
  		redirect_to :back, error: 'Error to Deleted'
  	end
  end

end
