class UsersController < FrontendController
  layout 'detail'

  def show
    user
  	@lists = properties.latest.page(page).per(per_page)
  end

  def reviews
    user
    properties
    rating(user.id)
    render file: 'users/reviews'
  end

  def delete_reviews
    rate = Rate.find(params[:rate_id])
    if rate.delete
      flash[:success] = 'Your Rate Successfully Deleted'
    else
      flash[:error] = rate.errors.full_message
    end
    redirect_to reviews_user_path(params[:id])
  end

  def about_company
    user
  end

  protected
    def user
      @user ||= User.find(params[:id])
    end

    def properties
      @properties ||= if user.developer?
        properties = user.projects
      else
        properties = user.properties
      end
    end

    def rating(user_id)
      @reviews = Rate.where(rateable_id: user_id).page(page).per(per_page)
    end
end
