class PublicsController < FrontendController

  def home
    @supporteds = Supported.all
    @testimonials = Testimonial.activated.limit(10)
    @regions = Region.featured.alfa.limit(6)
	end

  def package
    @packages = Package.actived.oldest
  end

  def get_categories
    property_type = PropertyType.find_by(name: params[:name])
    @get_categories = property_type ? property_type.children.alfa : PropertyType.none
    render layout: false
  end

  def get_societies(options={})
    category = params[:category]

    if category
      @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{category}%')")
      render partial: 'publics/facilities_new', layout: false
    end
  end

  def cities_tag
    province = Region.find_by(name: params[:name])
    @cities = province ? province.children : Region.none
    render layout: false
  end

end
