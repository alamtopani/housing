class Developerpage::DevelopersController < Developerpage::ApplicationController
	defaults resource_class: Developer, collection_name: 'developers', instance_name: 'developer'
  load_and_authorize_resource
  
	add_breadcrumb "Dashboard", :developerpage_dashboard_path
  add_breadcrumb "Users"

  def update
  	update! do |format|
  		if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
  	end
  end
end
