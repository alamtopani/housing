class Developerpage::ProjectsController < Developerpage::ApplicationController
  include ActionView::Helpers::NumberHelper
	defaults resource_class: Project, collection_name: 'projects', instance_name: 'project'
  load_and_authorize_resource

	add_breadcrumb "Dashboard", :developerpage_dashboard_path
  add_breadcrumb "Projects", :collection_path

  before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_types
  before_filter :prepare_facilities_edit, only: [:edit, :update]
  before_filter :prepare_facilities_new, only: [:new]

	def index
		@projects = current_user.projects.latest
		@collection = @projects.page(page).per(per_page)
	end

	def create
		build_resource
		resource.user_id = current_user.id
    resource.facilities = params[:facilities]
		create! do |format|
      if resource.errors.empty?
        UserMailer.send_request_project(resource).deliver
        format.html {redirect_to collection_path}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
	end

	def update
    resource.facilities = params[:facilities]
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end

		def prepare_map_location
      if params[:action] == "edit" || params[:action] == "update"
        village_id = resource.address.village_id
        @others = Project.joins(:address).where("projects.id != ?", resource.id).filter_by_village_id(village_id).check_latlang.map{|l| [l.title, l.address.latitude, l.address.longitude, l.price.to_i, l.type_project, l.first_image(:small), number_to_currency_br(l.price_in_meter)]}
      else
        @others = []
      end
		end

    def number_to_currency_br(number)
      number = 0 if number.blank?
      number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
    end

    def prepare_types
      @types = PropertyType.find_by(name: 'New Project').children.pluck(:name)
    end

		def prepare_facilities_edit
      @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{resource.type_project}%')")
    end

    def prepare_facilities_new
      @facilities = AssetFacility.latest
    end
end
