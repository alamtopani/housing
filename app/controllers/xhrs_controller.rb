class XhrsController < FrontendController
  include ActionView::Helpers::NumberHelper
  autocomplete :region, :name, display_value: :display_name

  def autocomplete_region
    term = params[:term]
    lists = []
    items = Region.from_depth(1).where('lower(name) LIKE lower(:term)', {term: "%#{term}%"}).limit(4)
    items.each do |item|
      lists << item
      lists << childrens_list(item) if item.has_children?
    end
    lists = lists.flatten.take(20) if lists.present?
    render json: json_for_autocomplete(lists, :display_name, {}), root: false
  end

  def autocomplete_auction_region
    term = params[:term]
    lists = []
    items = Region.from_depth(1).where('lower(name) LIKE lower(:term)', {term: "%#{term}%"}).limit(4)
    items.each do |item|
      lists << item
      lists << childrens_list(item) if item.has_children?
    end
    lists = lists.flatten.take(20) if lists.present?
    render json: json_for_autocomplete(lists, :display_name, {}), root: false
  end

  def childrens_list(item)
    child_list = []
    item.children.each do |child|
      child_list << child
    end
    child_list
  end

	def get_categories
    property_type = PropertyType.find_by(name: params[:name])
    @get_categories = property_type ? property_type.children.alfa : PropertyType.none
    render layout: false
  end

  def get_facilities(options={})
    category = params[:category]
    if category.present?
      @facility_list = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{category}%')")
      render partial: 'xhrs/facilities', layout: false
    end
  end

  def cities
    province = Region.find_by(id: params[:id])
    @cities = province ? province.children : Region.none
    render layout: false
  end

  def subdistricts
    city = Region.find_by(id: params[:id])
    @sub_districts = city ? city.children : Region.none
    render layout: false
  end

  def village
    sub = Region.find_by(id: params[:id])
    @villages  = sub ? sub.children : Region.none
    render layout: false
  end

  def area
    sub = Region.find_by(id: params[:id])
    @areas  = sub ? sub.children : Region.none
    render layout: false
  end

  def get_by_city
    city_id = params[:city_id]
    village_id = params[:village_id]
    region = Region.find_by(id: village_id)
    if params[:property_id]
      type = params[:type]
      data = Property.find_by(id: params[:property_id])
      @other = Property.filter_by_village_id(village_id).filter_by_type(type).where("properties.id != ?", params[:property_id]).map{|l| [l.title, l.address.latitude, l.address.longitude, "#{number_to_currency_br(l.the_price?)} #{l.the_price_status?}", l.type_property, l.category, l.first_image(:small), l.detail_property, number_to_currency_br(l.price_for_meter)]}
    elsif params[:location_id]
      # type = params[:type]
      data = Location.find_by(id: params[:location_id])
      # @other = Location.filter_by_village_id(village_id).filter_by_type(type).where("locations.id != ?", params[:location_id]).map{|l| [l.title, l.latitude, l.longitude, l.type_asset]}
      @other = Location.filter_by_village_id(village_id).where("locations.id != ?", params[:location_id]).map{|l| [l.title, l.latitude, l.longitude, l.type_asset]}
    elsif params[:project_id]
      data = Project.find_by(id: params[:project_id])
      @other = Project.filter_by_village_id(village_id).where("projects.id != ?", params[:project_id]).map{|l| [l.title, l.address.latitude, l.address.longitude, l.price.to_i, l.type_project, l.first_image(:small), number_to_currency_br(l.price_in_meter)]}
    end

    render json: {resource: data, address: data.try(:address), region: region, default_region: region.parent.parent, assets: @other }
  end

  def get_assets
    property = Property.find_by(id: params[:place_id])
    subdistrict_id_property = property.address.subdistrict_id
    @assets = Location.filter_by_subdistrict_id_limit_by_group(property.id, subdistrict_id_property)
    address = property.address.address
    render json: {assets: @assets, facilities: property.facilities, profile: property.basic_property, others: {address: address, resource: property} }
  end

  def get_assets_property
    property = Property.where(id: params[:place_id]).first
    subdistrict_id_property = property.address.subdistrict_id
    @assets = Location.filter_by_subdistrict_id_limit_by_group(property.id, subdistrict_id_property)
    render json: {asset: @assets, location: @assets.map(&:location)}
  end

  def save_assets_property
    property_id = Property.where(id: params[:place_id]).first.id
    location_id = Location.where(id: params[:location_id]).first.id
    asset_order = AssetProperty.find_or_initialize_by({ location_id: location_id,
                                        property_id: property_id,
                                        type_asset: params[:type_asset]
                                       })
    asset_order.distance = params[:distance]
    asset_order.duration = params[:duration]
    asset_order.save
    render nothing: true
  end

  def get_properties_search
    reg = params[:province].present? ? params[:province] : params[:city].present? ? params[:city] : "Kab. Aceh Singkil"
    @region = Region.where(name: reg).first
    @list_properties ||= Property.activated.search_by(params)
    render json: {region: @region, locations: @list_properties.map{|l| [l.title, l.address.latitude, l.address.longitude, l.price.to_i, l.id]} }
  end

  def get_project_assets
    project = Project.find_by(id: params[:place_id])
    subdistrict_id_project = project.address.subdistrict_id
    @assets = Location.filter_by_subdistrict_id_limit_by_group(project.id, subdistrict_id_project)
    render json: @assets
  end

  def get_assets_projects
    project = Project.where(id: params[:place_id]).first
    subdistrict_id_project = project.address.subdistrict_id
    @assets = Location.filter_by_subdistrict_id_limit_by_group(project.id, subdistrict_id_project)
    render json: {asset: @assets, location: @assets.map(&:location)}
  end

  def save_assets_project
    project_id = Project.where(id: params[:place_id]).first.id
    location_id = Location.where(id: params[:location_id]).first.id
    asset_order = AssetProject.find_or_initialize_by({ location_id: location_id,
                                        project_id: project_id,
                                        type_asset: params[:type_asset]
                                       })
    asset_order.distance = params[:distance]
    asset_order.duration = params[:duration]
    asset_order.save
    render nothing: true
  end

  def get_detail_properties
    property = Property.find_by(id: params[:id])
    user = property.user
    profile = user.profile
    address = property.address
    render partial: 'content_property_modal', locals: {resource: property, profile: profile, address: address}, layout: false
  end

  def get_detail_projects
    property = Project.find_by(id: params[:id])
    user = property.user
    profile = user.profile
    address = property.address
    render partial: 'content_project_modal', locals: {resource: property, profile: profile, address: address}, layout: false
  end

  def label_packages
    @label_packages = User.find(params[:id]).label_packages
    render layout: false
  end

  def number_to_currency_br(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end

end
