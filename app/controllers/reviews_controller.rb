class ReviewsController < FrontendController
  def create
    if user_signed_in?
      rate = params[:rate]
      obj = rate[:klass].classify.constantize.find(rate[:id])
      obj.rate_new rate[:score].to_f, current_user, rate[:dimension], rate[:review], params[:score]

      redirect_to :back
      flash[:success] = "Your Review Successfully Saved"
    else
      redirect_to :back
      flash[:error] = "You Must Login First"
    end
  end

end
