class Backend::AssetFacilitiesController < Backend::ApplicationController
	defaults resource_class: AssetFacility, collection_name: 'asset_facilities', instance_name: 'asset_facility'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Asset Facilities", :collection_path

  before_filter :prepare_categories

  include MultipleAction

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def create
		build_resource
		resource.category = params[:categories]
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	def update
		resource.category = params[:categories]
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end

		def prepare_categories
			@categories = ['House', 'Apartment','Ruko','Factory','Land','Office','Warehouse','Commercial Business - Business Unit']
		end
end
