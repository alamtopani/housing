class Backend::UsersController < Backend::ApplicationController
	defaults resource_class: User, collection_name: 'users', instance_name: 'user'
	before_filter :prepare_selects!, only: [:new, :edit, :create, :update]
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Users", :collection_path

	def index
		@collection = collection.search(params[:search]).latest.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end

		def prepare_selects!
	    @cities_selects = City.all
		end
end
