class Backend::ProjectsController < Backend::ApplicationController
  include ActionView::Helpers::NumberHelper
	defaults resource_class: Project, collection_name: 'projects', instance_name: 'project'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Projects", :collection_path

  before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_user
  before_filter :prepare_types
  before_filter :prepare_facilities_edit, only: [:edit, :update]
  before_filter :prepare_facilities_new, only: [:new]

  include MultipleAction

	def index
    if 1 === params[:export].to_i
      params[:per_page] = -1
      params[:bonds] = 'export'
      collection_data = Project.search_by(params)
      cols = export(collection_data)
      respond_to do |format|
        format.xls { send_data cols,filename: "data_projects_#{Time.now.to_i}.xls" }
      end
    else
  		@collection = collection.latest.page(page).per(per_page)
    end
	end

	def create
		build_resource
    resource.facilities = params[:facilities]
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	def update
    resource.facilities = params[:facilities]
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

  def import
  end

  def do_import
    if params[:file].present?
      options = {file: params[:file]}
      options.merge!(params.slice(:start_row, :columns).deep_symbolize_keys)
      ImportWorker.perform_by_env(ImportModel::Project.name, options)
      flash[:notice] = 'Your import is enqueued, Your Import Data Will Execute later'
    end
    redirect_to backend_projects_path
  end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params)
		end

    def prepare_types
      @types = PropertyType.find_by(name: 'New Project').children.pluck(:name)
    end

		def prepare_user
			@users = Developer.alfa.pluck(:username, :id)
		end

		def prepare_map_location
			if params[:action] == "edit" || params[:action] == "update"
         city = resource.address.city
         @others = Project.joins(:address).where("projects.id != ?", resource.id).filter_by_city(city).check_latlang.map{|l| [l.title, l.address.latitude, l.address.longitude, l.price.to_i, l.type_project, l.first_image(:small), number_to_currency_br(l.price_in_meter)]}
       else
         @others = []
       end
		end

    def number_to_currency_br(number)
      number = 0 if number.blank?
      number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
    end

    def prepare_facilities_edit
      @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{resource.type_project}%')")
    end

    def prepare_facilities_new
      @facilities = AssetFacility.latest
    end
end
