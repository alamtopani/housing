class Backend::ResourcesController < InheritedResources::Base
	include Parameters

	def create
		create! do |success, failure|
			success.html { redirect_to collection_path }
			failure.html do
				flash[:error] = resource.errors.full_messages
				redirect_to :back
			end
		end
	end

	def update
		update! do |success, failure|
			success.html { redirect_to collection_path }
		end
	end

  def export(collection)
    ResourceSupport::Exporter.new(collection).
      send("to_csv_#{collection.name.downcase}")
  end
end
