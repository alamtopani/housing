class Backend::SupportedsController < Backend::ApplicationController
  defaults resource_class: Supported, collection_name: 'supporteds', instance_name: 'supported'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Supporteds", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
