class Backend::AgentsController < Backend::ApplicationController
	defaults resource_class: Agent, collection_name: 'agents', instance_name: 'agent'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Agents", :collection_path

  include MultipleAction

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params)
		end
end
