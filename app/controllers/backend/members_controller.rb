class Backend::MembersController < Backend::ApplicationController
	defaults resource_class: Member, collection_name: 'members', instance_name: 'member'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Members", :collection_path

  include MultipleAction

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params)
		end
end
