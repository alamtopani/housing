class Backend::LandingPagesController < Backend::ApplicationController
  defaults resource_class: LandingPage, collection_name: 'landing_pages', instance_name: 'landing_page'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Landing Pages", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
