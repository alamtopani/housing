class Backend::LocationsController < Backend::ApplicationController
	defaults resource_class: Location, collection_name: 'locations', instance_name: 'location'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Locations", :collection_path

  before_filter :prepare_map_location, except: [:index, :destroy]

  include MultipleAction

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def create
		build_resource
    resource.user_id = current_user.id
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	def update
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params)
		end

		def prepare_map_location
			if params[:action] == "edit" || params[:action] == "update"
         city = resource.address.city
        @others = Location.joins(:address).where("locations.id != ?", resource.id).filter_by_city(city).map{|l| [l.title, l.latitude, l.longitude, l.type_asset]}
       else
         @others = []
       end
		end
end
