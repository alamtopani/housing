class Backend::WebSettingsController < Backend::ApplicationController
  defaults resource_class: WebSetting, collection_name: 'web_settings', instance_name: 'web_setting'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Web Settings", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
