class Backend::PropertyTypesController < Backend::ApplicationController
	defaults resource_class: PropertyType, collection_name: 'property_types', instance_name: 'property_type'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Property Types", :collection_path

  include MultipleAction

	def index
		@collection = collection.roots.alfa.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end
