class Backend::ApplicationController < Backend::ResourcesController
	include TheRole::Controller
  layout 'backend'

	before_filter :authenticate_admin!, :prepare_count
	protect_from_forgery with: :exception

	def authenticate_admin!
  	unless current_user.present? && (current_user.is_admin?)
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  protected
    def prepare_count
      @members_size = Member.all.size
      @agents_size = Agent.all.size
      @developers_size = Developer.all.size
      @properties_size = Property.all.size
      @projects_size = Project.all.size
      @balance_order_user_package = UserPackage.payed.where.not(price: nil).pluck(:price)
    end

    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end
end
