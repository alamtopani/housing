class Backend::DevelopersController < Backend::ApplicationController
	defaults resource_class: Developer, collection_name: 'developers', instance_name: 'developer'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Developers", :collection_path

  include MultipleAction

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params)
		end
end
