class Backend::PropertiesController < Backend::ApplicationController
  include ActionView::Helpers::NumberHelper
  defaults resource_class: Property, collection_name: 'properties', instance_name: 'property'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Properties", :collection_path

  before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_user
  before_filter :prepare_types
  before_filter :prepare_facilities_edit, only: [:edit, :update]
  before_filter :prepare_facilities_new, only: [:new]

  include MultipleAction

	def index
    if 1 === params[:export].to_i
      params[:per_page] = -1
      params[:bonds] = 'export'
      collection_data = Property.search_by(params).
                        where.not(category: 'Auction Property')
      cols = export(collection_data)
      respond_to do |format|
        format.xls { send_data cols, filename: "data_properties_#{Time.now.to_i}.xls" }
      end
    else
      @collection = collection.where.not(category: 'Auction Property')
    end
	end

	def create
		build_resource
    resource.facilities = params[:facilities]
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	def update
    resource.facilities = params[:facilities]
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

  def import
    @property_types = PropertyType.roots.where.not(name: 'New Project').pluck(:name)
  end

  def do_import
    if params[:file].present?
      options = {listing_type: params[:category], file: params[:file]}
      options.merge!(params.slice(:listing_type, :start_row, :columns).deep_symbolize_keys)
      ImportWorker.perform_by_env(ImportModel::Property.name, options)
      flash[:notice] = 'Your import is enqueued, Your Import Data Will Execute later'
    end
    redirect_to backend_properties_path
  end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params).latest.page(page).per(per_page)
		end

    def prepare_types
      @types = PropertyType.roots.where.not(name: 'New Project').where.not(name: 'Auction Property').pluck(:name)
    end

		def prepare_user
			@users = User.not_developer.alfa.pluck(:username, :id)
		end

		def prepare_map_location
			if params[:action] == "edit" || params[:action] == "update"
         city = resource.address.city
         @others = Property.joins(:address).where("properties.id != ?", resource.id).filter_by_city(city).check_latlang.map{|l| [l.title, l.address.latitude, l.address.longitude, "#{number_to_currency_br(l.the_price?)} #{l.the_price_status?}", l.type_property, l.category, l.first_image(:small), l.detail_property, number_to_currency_br(l.price_for_meter)]}
       else
         @others = []
       end
		end

    def number_to_currency_br(number)
      number = 0 if number.blank?
      number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
    end

    def prepare_facilities_edit
      @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{resource.type_property}%')")
    end

    def prepare_facilities_new
      @facilities = AssetFacility.latest
    end
end
