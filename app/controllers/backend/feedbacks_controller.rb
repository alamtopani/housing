class Backend::FeedbacksController < Backend::ApplicationController
  defaults resource_class: Feedback, collection_name: 'feedbacks', instance_name: 'feedback'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Feedbacks", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
