class Backend::OrderUserPackagesController < Backend::ApplicationController
  defaults resource_class: UserPackage, collection_name: 'user_packages', instance_name: 'user_package'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Order User Packages", :collection_path

  def index
    collection
  end

  def confirmations
    up = UserPackage.find(params[:id])
    if up.confirmation
      redirect_to :back, notice: 'Confirmation Has Successfully'
    else
      redirect_to :back, error: 'Confirmation Has Failed'
    end
  end

  def show
    @user_package = UserPackage.find(params[:id])
  end

  def multiple_confirmations
    collect = UserPackage.where("id IN (?)", params[:ids])
    collect.map{ |order| order.confirmation }
    redirect_to :back, notice: 'All Confirmations Has Successfully'
  end

  protected
    def collection
      @collection ||= end_of_association_chain.not_free.latest.page(page).per(per_page)
    end
end
