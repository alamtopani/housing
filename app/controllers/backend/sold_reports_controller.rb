class Backend::SoldReportsController < Backend::ApplicationController
  defaults resource_class: SoldReport, collection_name: 'sold_reports', instance_name: 'sold_report'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Sales Reports", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def do_multiple_act
    if params[:ids].present?
      obj_model = collection.name.singularize.classify.constantize
      collect = obj_model.where("id IN (?)", params[:ids])

      if params[:commit] == 'destroy_all'
        do_destroy_all(collect)
      end
    else
      redirect_to :back, alert: 'Please Choice Selected!'
    end
  end

  def do_destroy_all(collect)
    collect.each do |catalog|
      @property = catalog.sold_reportable
      @property.update(sold_out: false)
      @property.save
      catalog.destroy
    end
    redirect_to :back, notice: 'Property Report Successfully Deleted!'
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
