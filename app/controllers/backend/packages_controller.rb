class Backend::PackagesController < Backend::ApplicationController
  defaults resource_class: Package, collection_name: 'packages', instance_name: 'package'
  before_action :role_required
  before_action :select_options, except: [:index, :show, :delete]

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Packages", :collection_path

  include MultipleAction

	def index
		collection
	end

  def create
    params[:package][:label_tags] = params[:package][:label_tags].join(',') if params[:package][:label_tags]
    create! do |success, failure|
      success.html { redirect_to collection_path }
      failure.html do
        flash[:error] = resource.errors.full_messages
        redirect_to :back
      end
    end
  end

  def update
    params[:package][:label_tags] = params[:package][:label_tags].join(',') if params[:package][:label_tags]
    update! do |success, failure|
      success.html { redirect_to collection_path }
    end
  end

	protected
		def collection
			@collection ||= end_of_association_chain.latest.page(page).per(per_page)
		end

    def select_options
      @label_types = LabelType.to_a
    end
end
