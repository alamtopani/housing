class Backend::RegionsController < Backend::ApplicationController
	defaults resource_class: Region, collection_name: 'regions', instance_name: 'region'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Regions", :collection_path

	def index
		@collection = collection.roots.alfa.page(page).per(per_page)
	end

	def create
		build_resource
    resource.parent_id = get_parent_id
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end

	end


	protected
		def get_parent_id
			params[:region][:village_id].present? ? params[:region][:village_id] : params[:region][:subdistrict_id].present? ? params[:region][:subdistrict_id] : params[:region][:city_id].present? ? params[:region][:city_id] : params[:region][:parent_id].present? ? params[:region][:parent_id] : nil
		end

		def collection
			@collection ||= end_of_association_chain
		end
end
