class Backend::RolesController < Backend::ApplicationController
  defaults resource_class: Role, collection_name: 'roles', instance_name: 'role'
  before_action :role_required

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Roles"

  before_filter :select_modules,  only: [:edit, :update]
  before_filter :role_find,      only: [:edit, :update, :destroy, :change, :role_export]
  # before_filter :owner_required, only: [:edit, :update, :destroy, :change]

  def role_export
    role_for_export = {
      @role.name => {
        title:       @role.title,
        description: @role.description,
        role_hash:   @role.to_hash
      }
    }

    role_for_export[:export_comment] = "EXPORT Role with name: *#{ @role.name }*"
    send_data role_for_export.to_json, filename: "TheRole_#{ @role.name }.json"
  end

  def export
    roles = Role.all

    role_for_exports  = roles.inject({}) do |hash, role|
      hash[role.name] = {
        title:       role.title,
        description: role.description,
        role_hash:   role.to_hash
      }
      hash
    end

    role_for_exports[:export_comment] = "EXPORT Roles: *#{ roles.map(&:name).join(', ') }*"
    send_data role_for_exports.to_json, filename: "TheRole_#{ roles.map(&:name).join('-') }.json"
  end

  def import
    roles_hash = params[:roles].try(:read)
    roles_hash = begin; JSON.parse roles_hash; rescue; {}; end
    roles_hash.except!('export_comment')

    if roles_hash.keys.empty?
      flash[:error] = t 'the_role.cant_be_imported'
    else
      roles_list = roles_hash.keys.join(', ')
      update_roles(roles_hash)
      flash[:notice] =  t 'the_role.imported_roles', { roles_list: roles_list }
    end

    redirect_to admin_roles_url
  end

  private
    def select_modules
      @modules = [['members', 'members'], ['agents', 'agents'], ['developers', 'developers'], ['properties', 'properties'], ['auction_properties', 'auction_properties'],
                  ['projects', 'projects'], ['property_types', 'property_types'],['testimonials', 'testimonials'], ['web_settings', 'web_settings'], ['order_user_packages', 'order_user_packages'],
                  ['asset_facilities', 'asset_facilities'], ['locations', 'locations'], ['regions', 'regions'], ['sales_reports', 'sales_reports'], ['packages', 'packages'],
                  ['landing_pages', 'landing_pages'], ['feedbacks', 'feedbacks'], ['reports', 'reports'], ['supporteds', 'supporteds'], ['sold_reports', 'sold_reports']]
      @permissions = ['index', 'new', 'edit', 'destroy']
    end

  protected

  def update_roles roles_hash
    roles_hash.except('export_comment').each_pair do |role_name, role_data|
      title     = role_data['title']
      descr     = role_data['description']
      role_hash = role_data['role_hash']

      role = Role.where(name: role_name).first_or_create(title: title, description: descr)
      role.update_role role_hash = role_hash
    end
  end

  def role_params
    params.require(:role).permit(:name, :title, :description, :the_role, :based_on_role)
  end

  def role_find
    @role = Role.find params[:id]

    # TheRole: You should define OWNER CHECK OBJECT
    # When editable object was found
    @owner_check_object = @role
  end
end