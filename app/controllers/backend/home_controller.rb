class Backend::HomeController < Backend::ApplicationController
	before_filter :prepare_counts!

	add_breadcrumb "Dashboard", :backend_dashboard_path

	def dashboard
	end

	private
		def prepare_counts!
	  end

end
