class Backend::AuctionPropertiesController < Backend::ApplicationController
  include ActionView::Helpers::NumberHelper
  defaults resource_class: AuctionProperty, collection_name: 'auction_properties', instance_name: 'auction_property'
  before_action :role_required

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Auction Properties", :collection_path

  before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_user
  before_filter :prepare_types
  before_filter :prepare_facilities_edit, only: [:edit, :update]
  before_filter :prepare_facilities_new, only: [:new]

  include MultipleAction

	def index
		@collection = collection
	end

	def create
		build_resource
    resource.facilities = params[:facilities]
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	def update
    resource.facilities = params[:facilities]
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
	end

	protected
		def collection
			@collection ||= end_of_association_chain.search_by(params).latest.page(page).per(per_page)
		end

    def prepare_types
      @types = PropertyType.roots.where(name: 'Auction Property').pluck(:name)
    end

		def prepare_user
			@users = User.alfa.pluck(:username, :id)
		end

		def prepare_map_location
			if params[:action] == "edit" || params[:action] == "update"
         city = resource.address.city
         @others = Property.joins(:address).where("properties.id != ?", resource.id).filter_by_city(city).check_latlang.map{|l| [l.title, l.address.latitude, l.address.longitude, "#{number_to_currency_br(l.the_price?)} #{l.the_price_status?}", l.type_property]}
       else
         @others = []
       end
		end

    def number_to_currency_br(number)
      number = 0 if number.blank?
      number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
    end

    def prepare_facilities_edit
      @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{resource.type_property}%')")
    end

    def prepare_facilities_new
      @facilities = AssetFacility.latest
    end
end







