class Backend::RoleSectionsController < Admin::RoleSectionsController
  before_filter :role_find, only: [:create, :create_field, :create_rule, :rule_on, :rule_off, :destroy, :destroy_rule]
  before_action :role_required

  def role_required
    role_access_denied unless current_user.has_role?(controller_name, action_name)
  end

  def owner_required
    @owner_check_object = @role
  end


   def create_field
    if @role.create_field params[:section_rule], params[:field_name]
      flash[:notice] = 'access field created'
    else
      flash[:error]  = 'access field error'
    end

    redirect_to_edit
  end


  protected
  def redirect_to_edit
    redirect_to edit_backend_role_path @role
  end


end