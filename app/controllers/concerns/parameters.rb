module Parameters
	extend ActiveSupport::Concern

	protected
		def permitted_params
			instance_params = params[resource_instance_name]
			if instance_params.present?
        delete_blank_key(instance_params, :password)
        delete_blank_key(instance_params, :password_confirmation)
      end
			params.permit(resource_instance_name => Permitable.controller(params[:controller]))
		end

		def delete_blank_key(opts, key)
      opts.delete(key) if opts[key].blank?
    end
end
