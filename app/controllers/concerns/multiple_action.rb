module MultipleAction
	extend ActiveSupport::Concern

	def do_multiple_act
		if params[:ids].present?
			obj_model = collection.name.singularize.classify.constantize
			collect = obj_model.where("id IN (?)", params[:ids])

			if params[:commit] == 'destroy_all'
				do_destroy_all(collect)
			elsif params[:commit] == 'verified'
				verified_multiple(collect)
			elsif params[:commit] == 'non_verified'
				non_verified_multiple(collect)
			elsif params[:commit] == 'activated'
				activated_multiple(collect)
			elsif params[:commit] == 'non_activated'
				non_activated_multiple(collect)
			elsif params[:commit] == 'featured'
				change_featured_status(collect)
			elsif params[:commit] == 'checked'
				change_checked_status(collect)
			end
		else
			redirect_to :back, alert: 'Please Choice Selected!'
		end
	end

	def do_destroy_all(collect)
		if collect.destroy_all
			redirect_to :back, notice: 'Successfully Destroyed!'
		else
			redirect_to :back, alert: 'Not Successfully Destroyed!'
		end
	end

	def verified_multiple(collect)
		collect.each do |catalog|
			if catalog.class.name == 'Agent'
				UserMailer.send_user_approve_to_agent(catalog).deliver
			elsif catalog.class.name == 'Member'
				UserMailer.send_user_approve_to_member(catalog).deliver
			elsif catalog.class.name == 'Developer'
				UserMailer.send_user_approve_to_developer(catalog).deliver
			end
			catalog.verified = true
			catalog.save
		end
		redirect_to :back, notice: "You verified multiple items successfully change!"
	end

	def non_verified_multiple(collect)
		collect.each do |catalog|
			catalog.verified = false
			catalog.save
		end
		redirect_to :back, notice: "You unverified multiple items successfully change!"
	end

	def activated_multiple(collect)
		collect.each do |catalog|
			if catalog.class.name == 'Developer'
				UserMailer.send_user_approve_to_developer(catalog).deliver
			elsif catalog.class.name == 'Property' || catalog.class.name == 'AuctionProperty'
				UserMailer.send_catalog_verified(catalog).deliver
				catalog.set_expired_time?
			elsif catalog.class.name == 'Project'
				UserMailer.send_project_verified(catalog).deliver
			end
			catalog.activated = true
			catalog.save
		end
		redirect_to :back, notice: "You verified multiple items successfully change!"
	end

	def non_activated_multiple(collect)
		collect.each do |catalog|
			catalog.activated = false
			catalog.save
		end
		redirect_to :back, notice: "You unverified multiple items successfully change!"
	end

	def change_featured_status(collect)
		collect.each do |catalog|
			if catalog.featured == false
				if catalog.class.name == 'Property' || catalog.class.name == 'AuctionProperty'
					UserMailer.send_catalog_featured(catalog).deliver
				elsif catalog.class.name == 'Project'
					UserMailer.send_project_featured(catalog).deliver
				end
				catalog.featured = true
				catalog.save
			else
				catalog.featured = false
				catalog.save
			end
		end

		redirect_to :back, notice: "You featured multiple items successfully change!"
	end

	def change_checked_status(collect)
		collect.each do |catalog|
			if catalog.status == false
				catalog.status = true
				catalog.save
			else
				catalog.status = false
				catalog.save
			end
		end

		redirect_to :back, notice: "You status multiple checked successfully change!"
	end

end
