class ScheduleVisitsController < FrontendController

  def create
    @schedule_visit = ScheduleVisit.new(permitted_params)
    respond_to do |format|
      if @schedule_visit.save
        format.html {redirect_to :back, notice: 'Your Schedule Visit was successfully sent'}
      else
        format.html {redirect_to :back, errors: @schedule_visit.errors.full_messages}
      end
    end
  end

  private

    def permitted_params
      params.require(:schedule_visit).permit(Permitable.controller(params[:controller]))
    end
end
