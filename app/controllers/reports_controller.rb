class ReportsController < FrontendController

	def create
		@report = Report.new(permitted_params)
		respond_to do |format|
      if @report.save
      	UserMailer.send_report(@report).deliver
        format.html {redirect_to :back, notice: 'Your report was successfully sent'}
      else
        format.html {redirect_to :back, errors: @report.errors.full_messages}
      end
    end
	end

	private

		def permitted_params
			params.require(:report).permit(Permitable.controller(params[:controller]))
		end
end
