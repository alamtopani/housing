class LandingPagesController < FrontendController

  def show
    @page = LandingPage.find(params[:id])
  end

end
