class ProjectsController < FrontendController
  impressionist actions: [:show]
	before_filter :prepare_map_types

	layout 'application_projects_map'

	def index
    if params[:search].present?
      prepare_facilities_search(params[:type_project]) if params[:type_project].present?
    end

		if params[:list].present?
      search = params[:region_display_name].split(',').first if params[:region_display_name]
      reg = search.present? ? search : params[:city].present? ? params[:city] : params[:province].present? ? params[:province] : "Kota Jakarta Pusat"
      params[:city] = params[:city].present? ? params[:city] : "Kota Jakarta Pusat" if params[:region_display_name].blank?
      @region = Region.where(name: reg).first
			@list_projects = Project.search_by(params).latest.activated
			render :map
		else
			projects = collection

      @projects_count = projects.size
			@projects = projects.page(page).per(per_page)
			@featureds = projects.featured.limit(5)
      respond_to do |format|
        format.html { render :list }
        format.js { render 'index.js'}
      end
		end
	end

	def show
		@project = Project.find(params[:id])
    impressionist(@project, 'message')
		@inquiry = Inquiry.new
    @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{@project.type_project}%')")
	end

  def featureds
    @featureds = Project.activated.featured.page(page).per(per_page)
    @latest_projects = Project.activated.latest.limit(10)
  end

	private
		def prepare_map_types
      @types_location = LocationType.to_a
			@types = PropertyType.find_by(name: 'New Project').children.map(&:name)
      @others = []
      # city = params[:city].present? ? params[:city] : "Aceh Barat"
      # @projects = Project.joins(:address).filter_by_city(city).check_latlang.map{|l| [l.id, l.title, l.address.latitude, l.address.longitude, l.price.to_i]}
		end

    def collection
      @collection ||= Project.search_by(params).latest.activated.page(page).per(per_page)
    end

    def prepare_facilities_search(type)
      @facility_list = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{type}%')")
    end

end
