class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include DeviseFilter

  before_filter :prepare_checked
  before_filter :landing_page
  before_filter :web_setting

  protected
    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end

    def web_setting
	    @setting = WebSetting.first
	  end

    def landing_page
      @company = LandingPage.where("category =?", "company")
      @explore = LandingPage.where("category =?", "explore")
      @follow  = LandingPage.where("category =?", "follow")
      @helps   = LandingPage.where("category =?", "help")
    end

    def prepare_checked
      if current_user
        @latest_inquiries = Inquiry.available.received(current_user.id)
      end
    end

end
