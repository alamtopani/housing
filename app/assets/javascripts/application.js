//= require frontend/jquery-2.1.3.min
//= require jquery_ujs
//= require frontend/bootstrap.min
//= require frontend/jquery-ui.min
//= require frontend/jquery-scrolltofixed-min
//= require frontend/pinterest-grid-plugin
//= require frontend/lightbox
//= require frontend/gmap3
//= require frontend/theme
//= require frontend/shareds/carousel
//= require frontend/shareds/sidebar-canvas-wrapper
//= require frontend/select2
//= require frontend/create_ads
//= require frontend/jquery.custom_ligthbox
//= require frontend/searching_properties
//= require frontend/jquery.downCount
//= require cocoon
//= require shareds/markerclusterer
//= require autocomplete-rails
//= require moment
//= require bootstrap-datetimepicker
//= require jquery.raty
//= require ratyrate
//= require frontend/flipster
//= require ahoy
//= require autonumeric
//= require analytics
//= require frontend/jquery.nicescroll


$(window).load(function(){
  $('.search-form-multiaction li a').each(function(){
    $(this).click(function(){
      $('.h-search-multiaction').html($(this).attr('data-value'));
    });
  });

  $(".nav_trigger").click(function() {
    $("body").toggleClass("show_sidebar");
    $(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); // toggle 2 classes in Jquery: http://goo.gl/3uQAFJ - http://goo.gl/t6BQ9Q
  });

  var bodyHeight = $(window).height();
  if( $(window).width() > 800 ) {
    $('.nav-sidebar-users-right').css('height', bodyHeight);
  };

  var pushRight = new Menu({
    wrapper: '#f-wrapper',
    type: 'push-right',
    menuOpenerClass: '.f-button2',
    maskId: '#c-mask'
  });

  var pushRightBtn = document.querySelector('#c-button--push-right');

  pushRightBtn.addEventListener('click', function(e) {
    e.preventDefault;
    pushRight.open();
  });
});

$(document).ready(function(){
  $(".list-side-property").niceScroll();
  $(".flipster").flipster({ style: 'carousel', start: 0 });
  $('#region_display_name').bind('railsAutocomplete.select', function(event, data){
    if($(this).parent().attr('class') != 'input-group'){
      $("#property-search").submit();
      document.search.submit.click();
    }
  });

  if($(window).width() < 990){
    if($(".property").length > 0){
      $.each($('.property'), function(index, element){
        action = element.dataset.action;
        $(element).attr('href', action);
      });
    }
    if($(".project").length > 0){
      $.each($('.project'), function(index, element){
        action = element.dataset.action;
        $(element).attr('href', action);
      });
    }
  }

  $('.listing-auction-wrapper').each(function(index){
    var expired = $(this).attr('data-expired');
    $(this).find('.countdown').downCount({
      date: expired
    }, function () {
      $(this).find('.countdown').text('Already TimeOut');
    });
  });

  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  });

  if($('.property-detail-countdown').attr('data-expired') != ""){
    $('.property-detail-countdown').each(function(index){
      var expired = $(this).attr('data-expired');
      $(this).find('.countdown-timer').downCount({
        date: expired
      }, function () {
        alert('Already TimeOut!');
      });
    });
  }
  ahoy.trackView();
})
