//= require backend/jquery
//= require jquery_ujs
//= require backend/bootstrap.min
//= require backend/jquery-ui.min
//= require backend/fullcalendar.min
//= require backend/jquery.rateit.min
//= require backend/jquery.prettyPhoto
//= require backend/jquery.slimscroll.min
//= require backend/jquery.dataTables.min
//= require backend/excanvas.min
//= require backend/jquery.flot
//= require backend/jquery.flot.resize
//= require backend/jquery.flot.pie
//= require backend/jquery.flot.stack
//= require backend/jquery.noty
//= require backend/themes/default
//= require backend/layouts/bottom
//= require backend/layouts/topRight
//= require backend/layouts/top
//= require backend/sparklines
//= require backend/jquery.cleditor.min
//= require backend/bootstrap-datetimepicker.min
//= require backend/jquery.onoff.min
//= require backend/filter
//= require backend/custom
//= require backend/charts
//= require backend/select2
//= require ckeditor/init
//= require ckeditor/config
//= require backend/configurations
// require backend/jquery.comments
//= require shareds/markerclusterer
//= require cocoon
//= require moment
//= require bootstrap-datetimepicker
//= require autonumeric

$(document).ready(function(){
  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  })
});
