$(document).ready(function () {
  identityFieldSearch();

  $("#property-search #btn-submit").on('click', function(){
    get_data_from_ajax("#property-search", '#loading');
    return false;
  })
  $("#sort_order_property li a").on('click', function(){
    get_data_from_ajax("#property-search", '#loading', $(this).html());
    // $.get($("#property-search").attr("action"), $("#property-search").serialize()+'&sort='+$(this).html(), null, "script");
    return false;
  })
  $("select#sort_order_property").on('change', function(){
    var input = $("<input>").attr("type", "hidden").attr("name", "sort").val( $(this).val() );
    $("#property-search-map").append(input);
    document.search.submit.click();
    return false;
  })
  //  Project
  $("#property-search #btn-submit").on('click', function(){
    get_data_from_ajax("#property-search", '#loading')
    return false;
  })
  $("#sort_order_project li a").on('click', function(){
    $.get($("#property-search").attr("action"), $("#property-search").serialize()+'&sort='+$(this).html(), null, "script");
    return false;
  })
  $("select#sort_order_project").on('change', function(){
    var input = $("<input>").attr("type", "hidden").attr("name", "sort").val( $(this).val() );
    $("#property-search-map").append(input);
    document.search.submit.click();
    return false;
  })

  // search Facilities Project
  $(".panel-for-search #type_project").on('change', function(){
    $.ajax({
      url: $('.panel-for-search .facility-search').data('url'),
      data: {category: $(this).val()}
    }).done(function(data){
      $('.panel-for-search .facility-search').html(data);
    })
  })

  // search Facilities Property
  $(".panel-for-search").on('change', '#type_property', function(){
    $.ajax({
      url: $('.panel-for-search .facility-search').data('url'),
      data: {category: $(this).val()}
    }).done(function(data){
      $('.panel-for-search .facility-search').html(data);
    })
  })

	$('.provinces_select_tag').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/cities_tag?name='+ $(this).val(), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  })

  $('#load-more').on('click', function(){
    var current = $(this).data('current');
    var limit = current + 10;
    hidden_els = $(".ul-list-property li.hidden")
    childrens = $(".ul-list-property").find(hidden_els)
    $.each(childrens, function(i, hid){
      if($(hid).data('id') <= limit){
        $(hid).removeClass('hidden');
      }
    });
    $("#load-more").data('current', limit);
    if(childrens.length < 1){
      $("#load-more").addClass('hidden');
    }
    return false
  })

  function get_data_from_ajax(el, image_el, sort){
    if($(image_el).length == 1){
      $(image_el).removeClass('hide');
    }
    if(sort == undefined){
      var data = $(el).serialize();
    }else{
      var data = $(el).serialize() + '&sort=' + sort;
    }
    $.ajax({
      url: $(el).attr('action'),
      dataType: 'script',
      data: data
    }).done(function(){
      $(image_el).addClass('hide');
    })
  }

	$('.categories_select_search').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/get_categories?name='+$(this).val(), function(result){
        $('.category-container').html(result);
        $('.category-container .select2').select2();
      })
    }
  });


  function identityFieldSearch(){
    var type = $('.type_properties_select_search');
    if(type.val() != '' && type.val() != 'undefined'){
      // $.ajax({
      //   url: $('.panel-for-search .facility-search').data('url'),
      //   data: {category: type.val()}
      // }).done(function(data){
      //   $('.panel-for-search .facility-search').html(data);
      // });
      if(type.val() == 'Land'){
        $('.unless-land').hide();
        $('.unless-land').appendTo('.panel-for-search');
      }else{
        $('.unless-land').show();
        $('.unless-land').appendTo('.opt-more-search');
      }
      clear_value();
    }
  }

  function clear_value(){
    var inputs = $('.search-more-form').find('input')
    for (var i = inputs.length - 1; i >= 0; i--) {
      $(inputs[i]).val('');
    };

    var selects = $('.search-more-form').find('select')
    for (var i = selects.length - 1; i >= 0; i--) {
      $(selects[i]).val($(selects[i]).prop('defaultSelected'))
    };
  }

  $('.category-container').on('change', '.type_properties_select_search', function(){
    CheckFacility($(this).val());

    if($(this).val() == 'Land'){
      $(".group-land-area").show();
      $('.group-price-meter').hide();
      $('.group-area').show();
      $('.unless-land').hide();
      $('.unless-land').appendTo('.panel-for-search');
    }else if($(this).val() == 'Office'){
      $('.unless-land').show();
      $('.unless-land').appendTo('.opt-more-search');
      $(".group-land-area").hide();
      $('.group-area').show();
      $('.group-price-meter').show();
      $(".group-bedroom").hide();
      $(".group-bathroom").hide();
    }else if($(this).val() == 'Apartment'){
      $('.unless-land').show();
      $('.unless-land').appendTo('.opt-more-search');
      $('.group-area').show();
      $('.group-price-meter').show();
      $(".group-land-area").hide();
      $(".group-bedroom").show();
      $(".group-bathroom").show();
    }else if($(this).val() == 'House'){
      $('.unless-land').show();
      $('.group-price-meter').show();
      $('.unless-land').appendTo('.opt-more-search');
      $('.group-area').show();
      $(".group-land-area").show();
      $(".group-bedroom").show();
      $(".group-bathroom").show();
    }else if($(this).val() == 'Ruko'){
      $('.unless-land').show();
      $('.group-price-meter').hide();
      $('.group-area').show();
      $('.unless-land').appendTo('.opt-more-search');
      $(".group-land-area").show();
      $(".group-bedroom").hide();
    }else{
      $('.group-area').show();
      $('.group-price-meter').hide();
      $('.unless-land').hide();
      $('.unless-land').appendTo('.panel-for-search');
      $(".group-land-area").show();
    }
    clear_value();
  });

});
// add event blur
  $(".custom-input-container .maximum-price").on("blur",function (event) {
    if(blurFlag == true || (blurFlag == true && $(".custom-input-container .minimum-price").val() != '')){
      $(this).parents().removeClass('open');
    }
  });

  $(".custom-input-container .minimum-price").on("blur",function (event) {
    if(blurFlag == true || $(".custom-input-container .minimum-price").val() == ''){
      $(this).parents().removeClass('open');
    }
  });
// custom Price
$('.custom-dropdown').on("click",function (e) {
  if(($('.custom-dropdown .label-min-price').html() != '' || e.toElement == $(".custom-dropdown .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown .for-minimum-price li").parent()[0]) && e.toElement != $(".custom-dropdown .custom-input-container .minimum-price")[0] ){
    $(".custom-dropdown .custom-input-suggestion").css('text-align', 'right');
    $(".custom-dropdown .custom-input-suggestion ul.for-maximum-price").css('display','block');
    $(".custom-dropdown .custom-input-suggestion ul.for-minimum-price").css('display','none');
  }else{
    $(".custom-dropdown .custom-input-suggestion").css('text-align', 'left');
    $(".custom-dropdown .custom-input-suggestion ul.for-minimum-price").css('display','block');
    $(".custom-dropdown .custom-input-suggestion ul.for-maximum-price").css('display','none');
  }
  // $(this).find('.dropdown-menu .custom-dropdown .custom-input-suggestion').eq(0).css({'text-align':'left'});
  // $(this).find('.dropdown-menu .custom-dropdown .custom-input-suggestion').children().eq(0).show();
  // $(this).find('.dropdown-menu .custom-dropdown .custom-input-suggestion').children().eq(1).hide();
  // $(this).find('.custom-dropdown .custom-input-container').children().eq(0).focus();

  e.preventDefault();
  if(e.toElement == $(".custom-dropdown .custom-input-container .minimum-price")[0] || e.toElement == $(".custom-dropdown .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".for-minimum-price li").parent()[0]){
    $(this).on('hide.bs.dropdown', function () {
      return false;
    });
  }else{
    $('.custom-dropdown').removeClass('open');
  }
});

$(".custom-dropdown .custom-input-container .maximum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterCustom();
});

$(".custom-dropdown .custom-input-container .minimum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterCustom();
});

$(".custom-dropdown .custom-input-container .maximum-price").on("blur",function (event) {
  // if(changedMaxPrice && isAjaxFilter)
  //   searchFilter(1);
});

  /* on focus input*/
$(".custom-dropdown .custom-input-container .minimum-price").focus(function(e){
  // changedMinPrice = false;
  $(".custom-dropdown .custom-input-suggestion").css('text-align', 'left');
  $(".custom-dropdown .custom-input-suggestion ul.for-minimum-price").css('display','block');
  $(".custom-dropdown .custom-input-suggestion ul.for-maximum-price").css('display','none');
  e.preventDefault();
});

$(".custom-dropdown .custom-input-container .maximum-price").focus(function(e){
  // changedMaxPrice = false;
  // setMaxPriceRange();
  $(".custom-dropdown .custom-input-suggestion").css('text-align', 'right');
  $(".custom-dropdown .custom-input-suggestion ul.for-maximum-price").css('display','block');
  $(".custom-dropdown .custom-input-suggestion ul.for-minimum-price").css('display','none');
  e.preventDefault();
});

/* on click for suggestion */
$('.custom-dropdown .custom-input-suggestion ul.for-minimum-price li a').mouseup(function(e){
  blurFlag = false;
  $('.custom-dropdown .label-clear-price').hide();

  if($(this).text() != '0')
    $('.custom-dropdown .custom-input-container .minimum-price').val($(this).text().replace('Rp.','').substr(1));
  else
    $('.custom-dropdown .custom-input-container .minimum-price').val($(this).text());

  formatPricesFilterCustom();
      // if(isAjaxFilter) searchFilter(1);
  $('.custom-dropdown .maximum-price').focus();
  e.preventDefault();
    //return false;
});

$('.custom-dropdown .custom-input-suggestion ul.for-maximum-price li a').click(function(e){
  $('.custom-dropdown .label-clear-price').hide();
  blurFlag = true;

  var suggestMaxPrice = $(this).text().replace('Rp.','').substr(1);
  suggestMaxPrice = $.trim(suggestMaxPrice.replace(/,/g, "").replace(/\./g, ""));

  if (!isNaN(suggestMaxPrice))
    $('.custom-dropdown .custom-input-container .maximum-price').val($(this).text().replace('Rp.','').substr(1));
  else
    $('.custom-dropdown .custom-input-container .maximum-price').val('');

  formatPricesFilterCustom();
      // if(isAjaxFilter) searchFilter(1);
  // $('.custom-dropdown').removeClass('open');
  e.preventDefault();
    //return false;
});

function formatPricesFilterCustom()
{
  var minPrice = $.trim($('.custom-dropdown .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  var maxPrice = $.trim($('.custom-dropdown .custom-input-container .maximum-price').val().replace(/,/g, "").replace(/\./g, ""));

  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);
  maxPrice = isNaN(maxPrice) ? 0 : (maxPrice.length < 1 ? 0 : maxPrice);

  var lblMin = '';
  var lblMax = '';

  $('.custom-dropdown .label-clear-price').hide();
  $('.custom-dropdown .label-max-price').text('');
  $('.custom-dropdown .label-min-price').text('');

  lblMin = $('.custom-dropdown .custom-input-container .minimum-price').val().split('.');
  lblMax = $('.custom-dropdown .custom-input-container .maximum-price').val().split('.');

if (lblMin.length > 3) {
    if (lblMin.length > 4)
      lblMin = $('.custom-dropdown .custom-input-container .minimum-price').val().substring(0, $('.custom-dropdown .custom-input-container .minimum-price').val().length - 12) + ' ' + billion;
    else
      lblMin = lblMin[0] + ' ' + billion;
  } else if (lblMin.length > 2) {
    lblMin = lblMin[0] + ' ' + million;
  } else {
    lblMin = $('.custom-dropdown .custom-input-container .minimum-price').val();
  }

  if (lblMax.length > 3) {
    if (lblMax.length > 4)
      lblMax = $('.custom-dropdown .custom-input-container .maximum-price').val().substring(0, $('.custom-dropdown .custom-input-container .maximum-price').val().length - 12) + ' ' + billion;
    else
      lblMax = lblMax[0] + ' ' + billion;
  } else if (lblMax.length > 2) {
    lblMax = lblMax[0] + ' ' + million;
  } else {
    lblMax = $('.custom-dropdown .custom-input-container .maximum-price').val();
  }
    if (minPrice > 0 && maxPrice > 0) {
      $('.custom-dropdown .label-min-price').text(currency + ' ' + lblMin).css('display','inline');
      $('.custom-dropdown .label-max-price').text(" - " + currency + ' ' + lblMax).css('display','inline');
    } else if (minPrice == 0 && maxPrice == 0) {
      $('.custom-dropdown .label-clear-price').show();
    } else {
      if (minPrice <= 1) {
        $('.custom-dropdown .label-min-price').text('Until' + ' ' + currency + ' ' + lblMax).css('display','inline');
        $('.custom-dropdown .label-max-price').text('').css('display','inline');
      } else {
        $('.custom-dropdown .label-min-price').text(currency + ' ' +lblMin + '+').css('display','inline');
        $('.custom-dropdown .label-max-price').text('').css('display','inline');
      }
    }
    return true;
}

function setMaxPriceRangeCustom()
{
  var valueStep = jQuery.extend({}, rangeStep);
  var minPrice = $.trim($('.custom-dropdown .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);

  if (minPrice == 0) {
    delete valueStep[0];
  } else {
    for(x in valueStep) {
      if (minPrice > valueStep[x] || valueStep[x] == minPrice) {
        delete valueStep[x];
      }
    }
  }

}

// custom Price Meter
$('.custom-dropdown-price-meter').on("click",function (e) {
  if(($('.custom-dropdown-price-meter .label-min-price').html() != '' || e.toElement == $(".custom-dropdown-price-meter .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-price-meter .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-price-meter .for-minimum-price li").parent()[0]) && e.toElement != $(".custom-dropdown-price-meter .custom-input-container .minimum-price")[0] ){
    $(".custom-dropdown-price-meter .custom-input-suggestion").css('text-align', 'right');
    $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-maximum-price").css('display','block');
    $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-minimum-price").css('display','none');
  }else{
    $(".custom-dropdown-price-meter .custom-input-suggestion").css('text-align', 'left');
    $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-minimum-price").css('display','block');
    $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-maximum-price").css('display','none');
  }
  // $(this).find('.dropdown-menu .custom-dropdown-price-meter .custom-input-suggestion').eq(0).css({'text-align':'left'});
  // $(this).find('.dropdown-menu .custom-dropdown-price-meter .custom-input-suggestion').children().eq(0).show();
  // $(this).find('.dropdown-menu .custom-dropdown-price-meter .custom-input-suggestion').children().eq(1).hide();
  // $(this).find('.custom-dropdown-price-meter .custom-input-container').children().eq(0).focus();

  e.preventDefault();
  if(e.toElement == $(".custom-dropdown-price-meter .custom-input-container .minimum-price")[0] || e.toElement == $(".custom-dropdown-price-meter .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".for-minimum-price li").parent()[0]){
    $(this).on('hide.bs.dropdown', function () {
      return false;
    });
  }else{
    $('.custom-dropdown-price-meter').removeClass('open');
  }
});

$(".custom-dropdown-price-meter .custom-input-container .maximum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterCustomMeter();
});

$(".custom-dropdown-price-meter .custom-input-container .minimum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterCustomMeter();
});

$(".custom-dropdown-price-meter .custom-input-container .maximum-price").on("blur",function (event) {
  // if(changedMaxPrice && isAjaxFilter)
  //   searchFilter(1);
});

  /* on focus input*/
$(".custom-dropdown-price-meter .custom-input-container .minimum-price").focus(function(e){
  // changedMinPrice = false;
  $(".custom-dropdown-price-meter .custom-input-suggestion").css('text-align', 'left');
  $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-minimum-price").css('display','block');
  $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-maximum-price").css('display','none');
  e.preventDefault();
});

$(".custom-dropdown-price-meter .custom-input-container .maximum-price").focus(function(e){
  // changedMaxPrice = false;
  // setMaxPriceRange();
  $(".custom-dropdown-price-meter .custom-input-suggestion").css('text-align', 'right');
  $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-maximum-price").css('display','block');
  $(".custom-dropdown-price-meter .custom-input-suggestion ul.for-minimum-price").css('display','none');
  e.preventDefault();
});

/* on click for suggestion */
$('.custom-dropdown-price-meter .custom-input-suggestion ul.for-minimum-price li a').mouseup(function(e){
  blurFlag = false;
  $('.custom-dropdown-price-meter .label-clear-price').hide();

  if($(this).text() != '0')
    $('.custom-dropdown-price-meter .custom-input-container .minimum-price').val($(this).text().replace('Rp.','').substr(1));
  else
    $('.custom-dropdown-price-meter .custom-input-container .minimum-price').val($(this).text());

  formatPricesFilterCustomMeter();
      // if(isAjaxFilter) searchFilter(1);
  $('.custom-dropdown-price-meter .maximum-price').focus();
  e.preventDefault();
    //return false;
});

$('.custom-dropdown-price-meter .custom-input-suggestion ul.for-maximum-price li a').click(function(e){
  $('.custom-dropdown-price-meter .label-clear-price').hide();
  blurFlag = true;

  var suggestMaxPrice = $(this).text().replace('Rp.','').substr(1);
  suggestMaxPrice = $.trim(suggestMaxPrice.replace(/,/g, "").replace(/\./g, ""));

  if (!isNaN(suggestMaxPrice))
    $('.custom-dropdown-price-meter .custom-input-container .maximum-price').val($(this).text().replace('Rp.','').substr(1));
  else
    $('.custom-dropdown-price-meter .custom-input-container .maximum-price').val('');

  formatPricesFilterCustomMeter();
      // if(isAjaxFilter) searchFilter(1);
  // $('.custom-dropdown-price-meter').removeClass('open');
  e.preventDefault();
    //return false;
});

function formatPricesFilterCustomMeter()
{
  var minPrice = $.trim($('.custom-dropdown-price-meter .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  var maxPrice = $.trim($('.custom-dropdown-price-meter .custom-input-container .maximum-price').val().replace(/,/g, "").replace(/\./g, ""));

  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);
  maxPrice = isNaN(maxPrice) ? 0 : (maxPrice.length < 1 ? 0 : maxPrice);

  var lblMin = '';
  var lblMax = '';

  $('.custom-dropdown-price-meter .label-clear-price').hide();
  $('.custom-dropdown-price-meter .label-max-price').text('');
  $('.custom-dropdown-price-meter .label-min-price').text('');

  lblMin = $('.custom-dropdown-price-meter .custom-input-container .minimum-price').val().split('.');
  lblMax = $('.custom-dropdown-price-meter .custom-input-container .maximum-price').val().split('.');

  // if (lblMin.length > 2) {
  //   lblMin = lblMin[0] + ' ' + million;
  // } else {
    lblMin = $('.custom-dropdown-price-meter .custom-input-container .minimum-price').val();
  // }

  // if (lblMax.length > 2) {
  //   lblMax = lblMax[0] + ' ' + million;
  // } else {
    lblMax = $('.custom-dropdown-price-meter .custom-input-container .maximum-price').val();
  // }

    if (minPrice > 0 && maxPrice > 0) {
      $('.custom-dropdown-price-meter .label-min-price').text(currency + ' ' + lblMin).css('display','inline');
      $('.custom-dropdown-price-meter .label-max-price').text(" - " + currency + ' ' + lblMax).css('display','inline');
    } else if (minPrice == 0 && maxPrice == 0) {
      $('.custom-dropdown-price-meter .label-clear-price').show();
    } else {
      if (minPrice <= 1) {
        $('.custom-dropdown-price-meter .label-min-price').text('Until' + ' ' + currency + ' ' + lblMax).css('display','inline');
        $('.custom-dropdown-price-meter .label-max-price').text('').css('display','inline');
      } else {
        $('.custom-dropdown-price-meter .label-min-price').text(currency + ' ' +lblMin + '+').css('display','inline');
        $('.custom-dropdown-price-meter .label-max-price').text('').css('display','inline');
      }
    }
    return true;
}

function setMaxPriceRangeCustomMeter()
{
  var valueStep = jQuery.extend({}, rangeStep);
  var minPrice = $.trim($('.custom-dropdown-price-meter .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);

  if (minPrice == 0) {
    delete valueStep[0];
  } else {
    for(x in valueStep) {
      if (minPrice > valueStep[x] || valueStep[x] == minPrice) {
        delete valueStep[x];
      }
    }
  }

}

// custom Land
$('.custom-dropdown-land').on("click",function (e) {
  if(($('.custom-dropdown-land .label-min-price').html() != '' || e.toElement == $(".custom-dropdown-land .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-land .for-minimum-price li").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-land .for-minimum-price li a").parent()[0]) && e.toElement != $(".custom-dropdown-land .custom-input-container .minimum-price")[0] ){
    $(".custom-dropdown-land .custom-input-suggestion").css('text-align', 'right');
    $(".custom-dropdown-land .custom-input-suggestion ul.for-maximum-price").css('display','block');
    $(".custom-dropdown-land .custom-input-suggestion ul.for-minimum-price").css('display','none');
  }else{
    $(".custom-dropdown-land .custom-input-suggestion").css('text-align', 'left');
    $(".custom-dropdown-land .custom-input-suggestion ul.for-minimum-price").css('display','block');
    $(".custom-dropdown-land .custom-input-suggestion ul.for-maximum-price").css('display','none');
  }
  // $(this).find('.dropdown-menu .custom-input-suggestion').eq(0).css({'text-align':'left'});
  // $(this).find('.dropdown-menu .custom-input-suggestion').children().eq(0).show();
  // $(this).find('.dropdown-menu .custom-input-suggestion').children().eq(1).hide();
  // $(this).find('.custom-input-container').children().eq(0).focus();

  e.preventDefault();
  if(e.toElement == $(".custom-dropdown-land .custom-input-container .minimum-price")[0] || e.toElement == $(".custom-dropdown-land .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-land .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-land .for-minimum-price li").parent()[0]){
    $(this).on('hide.bs.dropdown', function () {
      return false;
    });
  }else{
    $('.custom-dropdown-land').removeClass('open');
  }
});

$(".custom-dropdown-land .custom-input-container .maximum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterLand();
});

$(".custom-dropdown-land .custom-input-container .minimum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterLand();
});

$(".custom-dropdown-land .custom-input-container .maximum-price").on("blur",function (event) {
  // if(changedMaxPrice && isAjaxFilter)
  //   searchFilter(1);
});

  /* on focus input*/
$(".custom-dropdown-land .custom-input-container .minimum-price").focus(function(e){
  // changedMinPrice = false;
  $(".custom-dropdown-land .custom-input-suggestion").css('text-align', 'left');
  $(".custom-dropdown-land .custom-input-suggestion ul.for-minimum-price").css('display','block');
  $(".custom-dropdown-land .custom-input-suggestion ul.for-maximum-price").css('display','none');
  e.preventDefault();
});

$(".custom-dropdown-land .custom-input-container .maximum-price").focus(function(e){
  // changedMaxPrice = false;
  // setMaxPriceRange();
  $(".custom-dropdown-land .custom-input-suggestion").css('text-align', 'right');
  $(".custom-dropdown-land .custom-input-suggestion ul.for-maximum-price").css('display','block');
  $(".custom-dropdown-land .custom-input-suggestion ul.for-minimum-price").css('display','none');
  e.preventDefault();
});

/* on click for suggestion */
$('.custom-dropdown-land .custom-input-suggestion ul.for-minimum-price li').mouseup(function(e){
  blurFlag = false;
  $('.custom-dropdown-land .label-clear-price').hide();

  if($(this).find('a').data('value') != '0')
    $('.custom-dropdown-land .custom-input-container .minimum-price').val($(this).find('a').data('value'));
  else
    $('.custom-dropdown-land .custom-input-container .minimum-price').val($(this).find('a').data('value'));

  formatPricesFilterLand();
      // if(isAjaxFilter) searchFilter(1);
  $('.custom-dropdown-land .maximum-price').focus();
  e.preventDefault();
    //return false;
});

$('.custom-dropdown-land .custom-input-suggestion ul.for-maximum-price li').click(function(e){
  $('.custom-dropdown-land .label-clear-price').hide();
  blurFlag = true;

  var suggestMaxPrice = $(this).find('a').data('value');
  suggestMaxPrice = $.trim(suggestMaxPrice);
  if (!isNaN(suggestMaxPrice))
    $('.custom-dropdown-land .custom-input-container .maximum-price').val($(this).find('a').data('value'));
  else
    $('.custom-dropdown-land .custom-input-container .maximum-price').val('');

  formatPricesFilterLand();
      // if(isAjaxFilter) searchFilter(1);
  // $('.custom-dropdown').removeClass('open');
  e.preventDefault();
    //return false;
});

function formatPricesFilterLand()
{
  var minPrice = $.trim($('.custom-dropdown-land .custom-input-container .minimum-price').val());
  var maxPrice = $.trim($('.custom-dropdown-land .custom-input-container .maximum-price').val());
  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);
  maxPrice = isNaN(maxPrice) ? 0 : (maxPrice.length < 1 ? 0 : maxPrice);

  var lblMin = '';
  var lblMax = '';

  $('.custom-dropdown-land .label-clear-price').hide();
  $('.custom-dropdown-land .label-max-price').text('');
  $('.custom-dropdown-land .label-min-price').text('');

  lblMin = $('.custom-dropdown-land .custom-input-container .minimum-price').val();
  lblMax = $('.custom-dropdown-land .custom-input-container .maximum-price').val();

  if (minPrice > 0 && maxPrice > 0) {
    $('.custom-dropdown-land .label-min-price').text(lblMin +' m2').css('display','inline');
    $('.custom-dropdown-land .label-max-price').text(" - " + lblMax +' m2').css('display','inline');
  } else if (minPrice == 0 && maxPrice == 0) {
    $('.custom-dropdown-land .label-clear-price').show();
  } else {
    if (minPrice <= 1) {
      $('.custom-dropdown-land .label-min-price').text('Until' + ' ' + lblMax +' m2').css('display','inline');
      $('.custom-dropdown-land .label-max-price').text('').css('display','inline');
    } else {
      $('.custom-dropdown-land .label-min-price').text(lblMin +' m2' + '+').css('display','inline');
      $('.custom-dropdown-land .label-max-price').text('').css('display','inline');
    }
  }
  return true;
}

function setMaxPriceRangeLand()
{
  var valueStep = jQuery.extend({}, rangeStep);
  var minPrice = $.trim($('.custom-dropdown-land .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);

  if (minPrice == 0) {
    delete valueStep[0];
  } else {
    for(x in valueStep) {
      if (minPrice > valueStep[x] || valueStep[x] == minPrice) {
        delete valueStep[x];
      }
    }
  }

}

// custom Area
$('.custom-dropdown-area').on("click",function (e) {
  if(($('.custom-dropdown-area .label-min-price').html() != '' || e.toElement == $(".custom-dropdown-area .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-area .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-area .for-minimum-price li").parent()[0]) && e.toElement != $(".custom-dropdown-area .custom-input-container .minimum-price")[0] ){
    $(".custom-dropdown-area .custom-input-suggestion").css('text-align', 'right');
    $(".custom-dropdown-area .custom-input-suggestion ul.for-maximum-price").css('display','block');
    $(".custom-dropdown-area .custom-input-suggestion ul.for-minimum-price").css('display','none');
  }else{
    $(".custom-dropdown-area .custom-input-suggestion").css('text-align', 'left');
    $(".custom-dropdown-area .custom-input-suggestion ul.for-minimum-price").css('display','block');
    $(".custom-dropdown-area .custom-input-suggestion ul.for-maximum-price").css('display','none');
  }
  // $(this).find('.dropdown-menu .custom-input-suggestion').eq(0).css({'text-align':'left'});
  // $(this).find('.dropdown-menu .custom-input-suggestion').children().eq(0).show();
  // $(this).find('.dropdown-menu .custom-input-suggestion').children().eq(1).hide();
  // $(this).find('.custom-input-container').children().eq(0).focus();

  e.preventDefault();
  if(e.toElement == $(".custom-dropdown-area .custom-input-container .minimum-price")[0] || e.toElement == $(".custom-dropdown-area .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-area .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-area .for-minimum-price li").parent()[0]){
    $(this).on('hide.bs.dropdown', function () {
      return false;
    });
  }else{
    $('.custom-dropdown-area').removeClass('open');
  }
});

$(".custom-dropdown-area .custom-input-container .maximum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterArea();
});

$(".custom-dropdown-area .custom-input-container .minimum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterArea();
});

$(".custom-dropdown-area .custom-input-container .maximum-price").on("blur",function (event) {
  // if(changedMaxPrice && isAjaxFilter)
  //   searchFilter(1);
});

  /* on focus input*/
$(".custom-dropdown-area .custom-input-container .minimum-price").focus(function(e){
  // changedMinPrice = false;
  $(".custom-dropdown-area .custom-input-suggestion").css('text-align', 'left');
  $(".custom-dropdown-area .custom-input-suggestion ul.for-minimum-price").css('display','block');
  $(".custom-dropdown-area .custom-input-suggestion ul.for-maximum-price").css('display','none');
  e.preventDefault();
});

$(".custom-dropdown-area .custom-input-container .maximum-price").focus(function(e){
  // changedMaxPrice = false;
  // setMaxPriceRange();
  $(".custom-dropdown-area .custom-input-suggestion").css('text-align', 'right');
  $(".custom-dropdown-area .custom-input-suggestion ul.for-maximum-price").css('display','block');
  $(".custom-dropdown-area .custom-input-suggestion ul.for-minimum-price").css('display','none');
  e.preventDefault();
});

/* on click for suggestion */
$('.custom-dropdown-area .custom-input-suggestion ul.for-minimum-price li').mouseup(function(e){
  blurFlag = false;
  $('.custom-dropdown-area .label-clear-price').hide();
  if($(this).find('a').data('value') != '0')
    $('.custom-dropdown-area .custom-input-container .minimum-price').val($(this).find('a').data('value'));
  else
    $('.custom-dropdown-area .custom-input-container .minimum-price').val($(this).find('a').data('value'));

  formatPricesFilterArea();
      // if(isAjaxFilter) searchFilter(1);
  $('.custom-dropdown-area .maximum-price').focus();
  e.preventDefault();
    //return false;
});

$('.custom-dropdown-area .custom-input-suggestion ul.for-maximum-price li').click(function(e){
  $('.custom-dropdown-area .label-clear-price').hide();
  blurFlag = true;

  var suggestMaxPrice = $(this).find('a').data('value');

  if (!isNaN(suggestMaxPrice))
    $('.custom-dropdown-area .custom-input-container .maximum-price').val($(this).find('a').data('value'));
  else
    $('.custom-dropdown-area .custom-input-container .maximum-price').val('');

  formatPricesFilterArea();
      // if(isAjaxFilter) searchFilter(1);
  // $('.custom-dropdown').removeClass('open');
  e.preventDefault();
    //return false;
});

function formatPricesFilterArea()
{
  var minPrice = $.trim($('.custom-dropdown-area .custom-input-container .minimum-price').val());
  var maxPrice = $.trim($('.custom-dropdown-area .custom-input-container .maximum-price').val());

  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);
  maxPrice = isNaN(maxPrice) ? 0 : (maxPrice.length < 1 ? 0 : maxPrice);

  var lblMin = '';
  var lblMax = '';

  $('.custom-dropdown-area .label-clear-price').hide();
  $('.custom-dropdown-area .label-max-price').text('');
  $('.custom-dropdown-area .label-min-price').text('');

  lblMin = $('.custom-dropdown-area .custom-input-container .minimum-price').val();
  lblMax = $('.custom-dropdown-area .custom-input-container .maximum-price').val();
  if (minPrice > 0 && maxPrice > 0) {
    $('.custom-dropdown-area .label-min-price').text(lblMin +' m2').css('display','inline');
    $('.custom-dropdown-area .label-max-price').text(" - " + lblMax +' m2').css('display','inline');
  } else if (minPrice == 0 && maxPrice == 0) {
    $('.custom-dropdown-area .label-clear-price').show();
  } else {
    if (minPrice <= 1) {
      $('.custom-dropdown-area .label-min-price').text('Until' + ' ' + lblMax +' m2').css('display','inline');
      $('.custom-dropdown-area .label-max-price').text('').css('display','inline');
    } else {
      $('.custom-dropdown-area .label-min-price').text(lblMin +' m2' + '+').css('display','inline');
      $('.custom-dropdown-area .label-max-price').text('').css('display','inline');
    }
  }
  return true;
}

function setMaxPriceRangeArea()
{
  var valueStep = jQuery.extend({}, rangeStep);
  var minPrice = $.trim($('.custom-dropdown-area .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);

  if (minPrice == 0) {
    delete valueStep[0];
  } else {
    for(x in valueStep) {
      if (minPrice > valueStep[x] || valueStep[x] == minPrice) {
        delete valueStep[x];
      }
    }
  }

}

// custom Building
$('.custom-dropdown-building').on("click",function (e) {
  if(($('.custom-dropdown-building .label-min-price').html() != '' || e.toElement == $(".custom-dropdown-building .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-building .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-building .for-minimum-price li").parent()[0]) && e.toElement != $(".custom-dropdown-building .custom-input-container .minimum-price")[0] ){
    $(".custom-dropdown-building .custom-input-suggestion").css('text-align', 'right');
    $(".custom-dropdown-building .custom-input-suggestion ul.for-maximum-price").css('display','block');
    $(".custom-dropdown-building .custom-input-suggestion ul.for-minimum-price").css('display','none');
  }else{
    $(".custom-dropdown-building .custom-input-suggestion").css('text-align', 'left');
    $(".custom-dropdown-building .custom-input-suggestion ul.for-minimum-price").css('display','block');
    $(".custom-dropdown-building .custom-input-suggestion ul.for-maximum-price").css('display','none');
  }
  // $(this).find('.dropdown-menu .custom-input-suggestion').eq(0).css({'text-align':'left'});
  // $(this).find('.dropdown-menu .custom-input-suggestion').children().eq(0).show();
  // $(this).find('.dropdown-menu .custom-input-suggestion').children().eq(1).hide();
  // $(this).find('.custom-input-container').children().eq(0).focus();

  e.preventDefault();
  if(e.toElement == $(".custom-dropdown-building .custom-input-container .minimum-price")[0] || e.toElement == $(".custom-dropdown-building .custom-input-container .maximum-price")[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-building .for-minimum-price li a").parent()[0] || $(e.toElement).parent()[0] == $(".custom-dropdown-building .for-minimum-price li").parent()[0]){
    $(this).on('hide.bs.dropdown', function () {
      return false;
    });
  }else{
    $('.custom-dropdown-building').removeClass('open');
  }
});

$(".custom-dropdown-building .custom-input-container .maximum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterBuilding();
});

$(".custom-dropdown-building .custom-input-container .minimum-price").on("keyup",function (event) {
  var key = event.keyCode || event.which;
  if(key >= 37 && key <= 40) return;
  changedMaxPrice = true;
  $(this).val($(this).val());
  formatPricesFilterBuilding();
});

$(".custom-dropdown-building .custom-input-container .maximum-price").on("blur",function (event) {
  // if(changedMaxPrice && isAjaxFilter)
  //   searchFilter(1);
});

  /* on focus input*/
$(".custom-dropdown-building .custom-input-container .minimum-price").focus(function(e){
  // changedMinPrice = false;
  $(".custom-dropdown-building .custom-input-suggestion").css('text-align', 'left');
  $(".custom-dropdown-building .custom-input-suggestion ul.for-minimum-price").css('display','block');
  $(".custom-dropdown-building .custom-input-suggestion ul.for-maximum-price").css('display','none');
  e.preventDefault();
});

$(".custom-dropdown-building .custom-input-container .maximum-price").focus(function(e){
  // changedMaxPrice = false;
  // setMaxPriceRange();
  $(".custom-dropdown-building .custom-input-suggestion").css('text-align', 'right');
  $(".custom-dropdown-building .custom-input-suggestion ul.for-maximum-price").css('display','block');
  $(".custom-dropdown-building .custom-input-suggestion ul.for-minimum-price").css('display','none');
  e.preventDefault();
});

/* on click for suggestion */
$('.custom-dropdown-building .custom-input-suggestion ul.for-minimum-price li').mouseup(function(e){
  blurFlag = false;
  $('.custom-dropdown-building .label-clear-price').hide();
  if($(this).find('a').data('value') != '0')
    $('.custom-dropdown-building .custom-input-container .minimum-price').val($(this).find('a').data('value'));
  else
    $('.custom-dropdown-building .custom-input-container .minimum-price').val($(this).find('a').data('value'));

  formatPricesFilterBuilding();
      // if(isAjaxFilter) searchFilter(1);
  $('.custom-dropdown-building .maximum-price').focus();
  e.preventDefault();
    //return false;
});

$('.custom-dropdown-building .custom-input-suggestion ul.for-maximum-price li').click(function(e){
  $('.custom-dropdown-building .label-clear-price').hide();
  blurFlag = true;

  var suggestMaxPrice = $(this).find('a').data('value')

  if (!isNaN(suggestMaxPrice))
    $('.custom-dropdown-building .custom-input-container .maximum-price').val($(this).find('a').data('value'));
  else
    $('.custom-dropdown-building .custom-input-container .maximum-price').val('');

  formatPricesFilterBuilding();
      // if(isAjaxFilter) searchFilter(1);
  // $('.custom-dropdown').removeClass('open');
  e.preventDefault();
  blurFlag = true;
    //return false;
});

function formatPricesFilterBuilding()
{
  var minPrice = $.trim($('.custom-dropdown-building .custom-input-container .minimum-price').val());
  var maxPrice = $.trim($('.custom-dropdown-building .custom-input-container .maximum-price').val());

  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);
  maxPrice = isNaN(maxPrice) ? 0 : (maxPrice.length < 1 ? 0 : maxPrice);

  var lblMin = '';
  var lblMax = '';

  $('.custom-dropdown-building .label-clear-price').hide();
  $('.custom-dropdown-building .label-max-price').text('');
  $('.custom-dropdown-building .label-min-price').text('');

  lblMin = $('.custom-dropdown-building .custom-input-container .minimum-price').val();
  lblMax = $('.custom-dropdown-building .custom-input-container .maximum-price').val();

  if (minPrice > 0 && maxPrice > 0) {
    $('.custom-dropdown-building .label-min-price').text(lblMin +' building').css('display','inline');
    $('.custom-dropdown-building .label-max-price').text(" - " + lblMax +' building').css('display','inline');
  } else if (minPrice == 0 && maxPrice == 0) {
    $('.custom-dropdown-building .label-clear-price').show();
  } else {
    if (minPrice < 1) {
      $('.custom-dropdown-building .label-min-price').text('Until' + ' ' + lblMax +' building').css('display','inline');
      $('.custom-dropdown-building .label-max-price').text('').css('display','inline');
    } else {
      $('.custom-dropdown-building .label-min-price').text(lblMin +' building' + '+').css('display','inline');
      $('.custom-dropdown-building .label-max-price').text('').css('display','inline');
    }
  }
  return true;
}

function setMaxPriceRangeArea()
{
  var valueStep = jQuery.extend({}, rangeStep);
  var minPrice = $.trim($('.custom-dropdown-building .custom-input-container .minimum-price').val().replace(/,/g, "").replace(/\./g, ""));
  minPrice = isNaN(minPrice) ? 0 : (minPrice.length < 1 ? 0 : minPrice);

  if (minPrice == 0) {
    delete valueStep[0];
  } else {
    for(x in valueStep) {
      if (minPrice > valueStep[x] || valueStep[x] == minPrice) {
        delete valueStep[x];
      }
    }
  }

}
var blurFlag = true;
var changedMinPrice = false;
var changedMaxPrice = false;
var currency = 'Rp.';
var rangeStep = [0,50000000,100000000,200000000,500000000,1000000000,2000000000,5000000000,10000000000,20000000000,50000000000,100000000000,200000000000,500000000000,1000000000000,2000000000000,5000000000000,10000000000000,20000000000000,50000000000000];
var rangeSizeStep = [50,100,200,500,1000,2000,5000,10000,20000,50000,100000,200000,500000,1000000,2000000,5000000,10000000,20000000,50000000,100000000];
var million = 'Juta';
var billion = 'Milyar';