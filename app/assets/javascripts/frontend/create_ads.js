$(document).ready(function () {
  propertyCheck();

  $('.categories_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/get_categories?name='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.category-container').html(result);
        $('.category-container .select2').select2();
      });

      if($('.categories_select:checked').val() == 'Sell'){
        $('.price-sell').show();
        $('.price-sell').appendTo('.section-price-form');
      }else{
        $('.price-sell').hide();
        $('.price-sell').appendTo('.hidden-form');
      }

      if($('.categories_select:checked').val() == 'Auction Property'){
        $('.auction-form').show();
        $('.auction-form').appendTo('.section-auction-form');
      }else{
        $('.auction-form').hide();
        $('.auction-form').appendTo('.hidden-form');
      }
    }
  });

  if($('.categories_select:checked').val() == 'Sell'){
    SellCategoriesSelected($('.type_properties_select').val());
  }else if($('.categories_select:checked').val() == 'Rent'){
    RentCategoriesSelected($('.type_properties_select').val());
    $('.price-sell').hide();
    $('.price-sell').appendTo('.hidden-form');
  }else{
    SellCategoriesSelected($('.type_properties_select').val());
    $('.price-sell').hide();
    $('.price-sell').appendTo('.hidden-form');
  }

  if($('.categories_select:checked').val() == 'Auction Property'){
    $('.auction-form').show();
    $('.auction-form').appendTo('.section-auction-form');
  }else{
    $('.auction-form').hide();
    $('.auction-form').appendTo('.hidden-form');
  }

  $('.category-container').on('change', '.type_properties_select', function(){
    if($('.categories_select:checked').val() == 'Sell'){
      if($(this).val() != '' && $(this).val() != 'undefined'){
        SellCategoriesSelected($(this).val());
      }
    }else if($('.categories_select:checked').val() == 'Rent'){
      if($(this).val() != '' && $(this).val() != 'undefined'){
        RentCategoriesSelected($(this).val());
      }
    }else{
      if($(this).val() != '' && $(this).val() != 'undefined'){
        SellCategoriesSelected($(this).val());
      }
    }

    CheckFacility($(this).val());
  });

  $('.type_properties_select').on('change', function(){
    if($(this).val() == 'Apartment' || $(this).val() == 'Office Tower'){
      $('.square-area').html('Gross Area in m<sup>2</sup>');
    }else{
      $('.square-area').html('Area m<sup>2</sup>');
    }
  });

  var navListItems = $('div.setup-panel div a'),
  allWells = $('.setup-content'),
  allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function (e) {
    e.preventDefault();
    var $target = $($(this).attr('href')),
        $item = $(this);

    if (!$item.hasClass('disabled')) {
      navListItems.removeClass('btn-info').addClass('btn-default');
      $item.addClass('btn-info');
      allWells.hide();
      $target.show();
      $target.find('input:eq(0)').focus();
    }
  });

  allNextBtn.click(function(){
    var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input, input[type='text'],input[type='url'],input[type='radio'],input[type='checkbox'],input[type='number'],select,textarea"),
        isValid = true;

    $(".form-group").removeClass("has-error");
    for(var i=0; i<curInputs.length; i++){
      if($(curInputs[i]).attr("name") == "property[auction_attributes][start_auction]" && !$('.auction-form').is(":visible")) { continue; }
      if($(curInputs[i]).attr("name") == "property[auction_attributes][end_auction]" && !$('.auction-form').is(":visible")) { continue; }
      if (!curInputs[i].validity.valid){
        isValid = false;
        $(curInputs[i]).closest(".form-group").addClass("has-error");
        if($(curInputs[i]).closest(".form-group").find('.span-error').text().length == 0){
          $(curInputs[i]).closest(".form-group").append("<span class='span-error'>required</span>");
        }
      }
    }

    if (isValid)
      nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('.choice-gallery-form .input-file').on('change', function(index){
    if($(this).length){
      previewFile(this, $(this).parent('li').find('.show-choice'));
    }
  });

  $('div.setup-panel div a.btn-info').trigger('click');
});


function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

var rent = $('#property_category_rent');
var sell = $('#property_category_sell');
var auction_property = $('#property_category_auction_property');

function propertyCheck(){
  rent.click(function(){
    checkType(this);
  });

  sell.click(function(){
    checkType(this);
  });

  auction_property.click(function(){
    checkType(this);
  });

  if($(rent).length > 0 || $(sell).length > 0 || $(auction_property).length > 0){
    checkType(rent[0]);
    checkType(sell[0]);
    checkType(auction_property[0]);
  }
}

function checkType(type){
  if($(type).is(':checked')){
    if(type == rent[0]){
      $('.amenity').show();
    }else{
      $('.amenity').hide();
    }
  }
}

function SellCategoriesSelected(cate){
  if(cate == 'House'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-house').appendTo('.section-basic-form');
    $('.basic-form-house').show();
  }else if(cate == 'Apartment'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-apartment').appendTo('.section-basic-form');
    $('.basic-form-apartment').show();
  }else if(cate == 'Ruko'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-ruko').appendTo('.section-basic-form');
    $('.basic-form-ruko').show();
  }else if(cate == 'Factory'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-factory').appendTo('.section-basic-form');
    $('.basic-form-factory').show();
  }else if(cate == 'Land'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-land').appendTo('.section-basic-form');
    $('.basic-form-land').show();
  }else if(cate == 'Office'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-office').appendTo('.section-basic-form');
    $('.basic-form-office').show();
  }else if(cate == 'Warehouse'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-warehouse').appendTo('.section-basic-form');
    $('.basic-form-warehouse').show();
  }else if(cate == 'Commercial Business'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-unit').appendTo('.section-basic-form');
    $('.basic-form-unit').show();
  }
}

function RentCategoriesSelected(cate){
  if(cate == 'House'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-house').appendTo('.section-basic-form');
    $('.rent-basic-form-house').show();
  }else if(cate == 'Apartment'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-apartment').appendTo('.section-basic-form');
    $('.rent-basic-form-apartment').show();
  }else if(cate == 'Ruko'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-ruko').appendTo('.section-basic-form');
    $('.rent-basic-form-ruko').show();
  }else if(cate == 'Land'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-land').appendTo('.section-basic-form');
    $('.rent-basic-form-land').show();
  }else if(cate == 'Office'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-office').appendTo('.section-basic-form');
    $('.rent-basic-form-office').show();
  }else if(cate == 'Warehouse'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-warehouse').appendTo('.section-basic-form');
    $('.rent-basic-form-warehouse').show();
  }
}

function CheckFacility(category){
  $.each($('.facility'), function(index, element){
    var url = $(this).data('url') + "?category=" + category;
    var that = $(this);
    $.ajax({
      type: "GET",
      url: url,
      success: function(data){
        $(that).html(data);
      }
    });
  });
}

