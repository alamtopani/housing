class Member < User
  ratyrate_rateable 'happiness'
  ratyrate_rater
	extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  after_initialize :after_initialized_agent

	default_scope {where(type: Member.name)}

  has_one :agent_info, foreign_key: 'user_id', dependent: :destroy
  accepts_nested_attributes_for :agent_info, reject_if: :all_blank, allow_destroy: true


  def after_initialized_agent
    self.agent_info = AgentInfo.new if self.agent_info.blank?
  end
end
