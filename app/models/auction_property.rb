class AuctionProperty < Property
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  after_initialize :after_initialized
	default_scope {where(category: 'Auction Property')}

	private
		def after_initialized
			self.address ||= Address.new if self.new_record?
			self.basic_property ||= BasicProperty.new if self.new_record?
			self.auction ||= Auction.new if self.new_record?
		end
end
