class Facing < EnumerateIt::Base
  associate_values :West, :North, :East, :South
end
