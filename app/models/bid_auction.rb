class BidAuction < ActiveRecord::Base
	belongs_to :auction_property, foreign_key: 'property_id'
	belongs_to :user, foreign_key: 'user_id'

	scope :top_rank, ->{order(bid: :desc)}

	include ScopeBased
end
