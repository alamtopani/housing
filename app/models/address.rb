class Address < ActiveRecord::Base
  attr_accessor :area_name

  before_save :get_name_region
  belongs_to :addressable, polymorphic: true
	include ScopeBased

	def place_info
		[address, postcode, village, sub_district, city, province].select(&:'present?').join(', ')
	end

  def simple_place_info
    [village, sub_district, city].select(&:'present?').join(', ')
  end

  def place_info_user
    [address, postcode, city, province].select(&:'present?').join(', ')
  end

  def get_name_region
    self.province = Region.find(self.province_id).try(:name) if self.province_id
    self.city = Region.find(self.city_id).try(:name) if self.city_id
    self.sub_district = Region.find(self.subdistrict_id).try(:name) if self.subdistrict_id
    self.village = Region.find(self.village_id).try(:name) if self.village_id
    self.area = Region.find(self.area_id).try(:name) if self.area_id && !self.area_id.zero?
    update_region_and_set_area_id(self.area_name, self.village_id) if self.area_name.present? && self.village_id
  end

  def update_region_and_set_area_id(area_name, parent_id)
    reg = Region.new({name: area_name, parent_id: parent_id})
    reg.save
    self.area_id = reg.id
    self.area = reg.name
  end

end
