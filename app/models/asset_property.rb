class AssetProperty < ActiveRecord::Base
  belongs_to :location
  belongs_to :property
end