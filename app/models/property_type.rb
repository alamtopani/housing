class PropertyType < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  include ScopeBased
	include Tree

  scope :alfa, ->{order(name: :asc)}
  scope :just_child, ->{where.not(ancestry: nil)}
end

