class Wishlist < ActiveRecord::Base
  belongs_to :user, foreign_key: 'user_id'
  belongs_to :property, foreign_key: 'property_id'

  include ScopeBased
end
