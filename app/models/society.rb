class Society < ActiveRecord::Base
	belongs_to :societiable, polymorphic: true

	# default_scope { order('societies.position ASC') }
	include ScopeBased

	def exist?
		if self.available == true
			return 'Available'
		else
			return 'Not Available'
		end
	end

	def exist_hidden?
		if self.available == true
			return 'available'
		else
			return 'not_available'
		end
	end
end
