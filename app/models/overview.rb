class Overview < ActiveRecord::Base
	belongs_to :overviewable, polymorphic: true
	include ScopeBased
  default_scope { order('overviews.position ASC') }

  after_initialize :populate_interiors, :populate_amenities

  has_many :interiors, as: :interiorable, dependent: :destroy
  accepts_nested_attributes_for :interiors, reject_if: :all_blank, allow_destroy: true

  has_many :amenities, as: :amenitiable, dependent: :destroy
  accepts_nested_attributes_for :amenities, reject_if: :all_blank, allow_destroy: true

	has_attached_file :file_2d, styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :file_2d, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :file_3d, styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :file_3d, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  def build_interior(interior_title, val, category, position=nil)
    society = self.interiors.build({title: interior_title, val: val, category: category})
    society.position = position
  end

  def build_amenity(amenity_title, icon, position=nil)
    amenity = self.amenities.build({title: amenity_title, icon: icon})
    amenity.position = position
  end

  private
    def populate_interiors
      if self.interiors.length < 11
        [
          ['Balcony', 'Standard Floorings','FLOORINGS'],
          ['Living/Dining', 'Vitrified Tiles','FLOORINGS'],
          ['Other Bedroom', 'Vitrified Tiles','FLOORINGS'],
          ['Kitchen', 'Vitrified Tiles','FLOORINGS'],
          ['Master Bedroom', 'Vitrified Tiles','FLOORINGS'],
          ['Toilets', 'Anti Skid Tiles','FLOORINGS'],
          ['Doors', 'Standard Fittings','FITTINGS'],
          ['Kitchen', 'Granite Platform with SS Sink','FITTINGS'],
          ['Toilets', 'CP Fittings','FITTINGS'],
          ['Electrical', 'Concealed Copper Wirings with Modular Switches','FITTINGS'],
          ['Windows','Anodized Sliding Windows','FITTINGS'],
          ['Others','Standard Fittings','FITTINGS'],
          ['Exterior','Standard Paints','WALLS'],
          ['Kitchen','Dado Tiles','WALLS'],
          ['Interior','Acrylic Paints','WALLS'],
          ['Toilets','Dado Tiles','WALLS']
        ].each_with_index do |interior_title, index|
          _interior = self.interiors.select{|g| g.title[0].to_s.downcase == interior_title[0].downcase}.first
          unless _interior
            self.build_interior(interior_title[0], interior_title[1], interior_title[2], index+1)
          else
            _interior.position = index+1
          end
        end
      end
    end

    def populate_amenities
      if self.amenities.length < 5
        [
          ['Living Room', '<i class="fa fa-bed"></i>'],
          ['Servant Room', '<i class="fa fa-child"></i>'],
          ['Kitchen','<i class="fa fa-filter"></i>'],
          ['Gas Pipeline', '<i class="fa fa-sliders"></i>'],
          ['Power Backup', '<i class="fa fa-lightbulb-o"></i>'],
        ].each_with_index do |amenity_title, index|
          _amenity = self.amenities.select{|g| g.title[0].to_s.downcase == amenity_title[0].downcase}.first
          unless _amenity
            self.build_amenity(amenity_title[0], amenity_title[1], index+1)
          else
            _amenity.position = index+1
          end
        end
      end
    end
end
