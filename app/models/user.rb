class User < ActiveRecord::Base
	extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  after_create :send_welcome_mail

  include Ratyrate::Model
	include TheUser::UserConfig
	include TheUser::UserAuthenticate
	include TheUser::UserSearching
	include ScopeBased

  validates_uniqueness_of :code, if: 'self.code.present?'

  def package_payed?(package_id)
    user_packages.where(package_id: package_id).payed.present?
  end

  def label_packages
    user_packages.payed.not_expired.pluck(:label_tags).flatten.compact.join(',').split(',').uniq
  end

  def type_package
    type_package = packages.last.name == "Free Package" ? 'Free' : 'Premium'
  end

  def package_expire?(package_id)
    user_packages.where(package_id: package_id).expired.present?
  end

  def package_not_payed?(package_id)
    user_packages.where(package_id: package_id).not_payed.present?
  end

  def remain_listing
    current_package.max_listing - properties.count
  end

  def remain_featured_listing
    current_package.featured_listing - properties.featured.count
  end

  private
    def send_welcome_mail
      UserMailer.welcome_join(self).deliver
    end

end
