class ReportCategory < EnumerateIt::Base
  associate_values :incorrect_information, :other
end
