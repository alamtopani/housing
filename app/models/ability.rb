class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.is_admin?
      can :manage, :all
    elsif user.member? || user.agent?
      can :manage, Member, id: user.id
      can :manage, Agent, id: user.id
      can :manage, Property, user_id: user.id
      # can :manage, Inquiry, user_id: user.id
      can :manage, Wishlist, user_id: user.id
      can :manage, SoldReport, user_id: user.id
      can :manage, Package
    elsif user.developer?
      can :manage, Developer, id: user.id
      can :manage, Project, user_id: user.id
      # can :manage, Inquiry, user_id: user.id
      can :manage, Wishlist, user_id: user.id
    end
  end
end
