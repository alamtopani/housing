class CategoryProperty < EnumerateIt::Base
  associate_values(
    :rent             					=> [1,'Rent'],
    :sell												=> [2,'Sell'],
    :auction_property      			=> [3,'Auction Property']
  )
  sort_by :value
end
