module TheProject
	module ProjectConfig
		extend ActiveSupport::Concern

		included do
			is_impressionable
			after_initialize :after_initialized, :populate_galleries, :populate_overviews #,:populate_societies
			before_save :prepare_price_in_meter
			before_create :prepare_code

			belongs_to :user, foreign_key: 'user_id'

			has_one :address, as: :addressable, dependent: :destroy
			accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true

		  # has_many :societies, as: :societiable, dependent: :destroy
		  # accepts_nested_attributes_for :societies, reject_if: :all_blank, allow_destroy: true

			has_many :galleries, as: :galleriable, dependent: :destroy
			accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

			has_many :overviews, as: :overviewable, dependent: :destroy
		  accepts_nested_attributes_for :overviews, reject_if: :all_blank, allow_destroy: true

		  has_one :inquiries, as: :inquiriable #,dependent: :destroy
			#accepts_nested_attributes_for :inquiries, reject_if: :all_blank, allow_destroy: true

			has_many :reports, as: :reportable, dependent: :destroy
			accepts_nested_attributes_for :reports, reject_if: :all_blank, allow_destroy: true

			scope :filter_by_city, ->(city) {joins(:address).where("LOWER(addresses.city) = LOWER(?)", city)}
			scope :filter_by_village_id, ->(village_id) {joins(:address).where("addresses.village_id = ?", village_id)}
			scope :check_latlang, ->{where.not("addresses.longitude =? and addresses.longitude =?", 'nil', 'nil')}
			scope :featured, ->{where("projects.featured =?", true)}
			scope :activated, ->{ where(activated: true) }
			scope :non_activated, ->{ where(activated: false) }
			scope :bonds, ->{
												eager_load(
																		{user: [:profile]},
																		:address
																	)
											}
		end

		def first_image(style)
			self.try(:galleries).try(:first).try(:file).url(style)
		end

		def build_image(media_title, position=nil)
	    gallery = self.galleries.build({title: media_title})
	    gallery.position = position
	  end

	  def build_society(society_title, icon, position=nil)
	    society = self.societies.build({title: society_title, icon: icon})
	    society.position = position
	  end

	  def build_overview(overview_title, position=nil)
	    overview = self.overviews.build({title: overview_title})
	    overview.position = position
	  end

	  def map_exist?
	  	self.address.latitude.present? && self.address.longitude.present?
	  end

	  def active?
	  	if self.activated == true
	  		return "<i class='fa fa-check'></i> Published".html_safe
	  	else
	  		return "<i class='fa fa-exclamation'></i> Not Published".html_safe
	  	end
	  end

	  def featured?
	  	if self.featured == true
	  		return "[Featured]".html_safe
	  	end
	  end

		private

			def prepare_price_in_meter
				if self.type_project != 'House'
					self.price_in_meter = self.price/self.area.to_i if self.area.present?
				else
					self.price_in_meter = nil
				end
			end

			def prepare_code
				self.code = SecureRandom.hex(3) if self.code.blank?
			end

			def after_initialized
				self.address ||= Address.new if self.new_record?
			end

			def populate_overviews
	      if self.overviews.length < 2
	        [
	          '1 Bedroom',
	          '2 Bedroom'
	        ].each_with_index do |media_title, index|
	          _overview = self.overviews.select{|g| g.title.to_s.downcase == media_title.downcase}.first
	          unless _overview
	            self.build_overview(media_title, index+1)
	          else
	            _overview.position = index+1
	          end
	        end
	      end
	    end

	    # def populate_societies
	    #   if self.societies.length < 14
	    #     [
	    #       ['Gym', '<i class="fa fa-chain"></i>'],
	    #       ['Swimming Pool', '<i class="fa fa-life-saver"></i>'],
	    #       ['Gas Pipeline', '<i class="fa fa-sliders"></i>'],
	    #       ['Power Backup', '<i class="fa fa-lightbulb-o"></i>'],
	    #       ['Water Supply', '<i class="fa fa-tint"></i>'],
	    #       ['Gated Community', '<i class="fa fa-inbox"></i>'],
	    #       ['Garden', '<i class="fa fa-tree"></i>'],
	    #       ['Kids Play Area', '<i class="fa fa-smile-o"></i>'],
	    #       ['Sports Facility', '<i class="fa fa-street-view"></i>'],
	    #       ['Lift', '<i class="fa fa-arrows-v"></i>'],
	    #       ['Parking','<i class="fa fa-car"></i>'],
	    #       ['Children Play Area','<i class="fa fa-smile-o"></i>'],
	    #       ['Security Guards','<i class="fa fa-shield"></i>'],
	    #       ['Cycling & Jogging','<i class="fa fa-bicycle"></i>']
	    #     ].each_with_index do |society_title, index|
	    #       _society = self.societies.select{|g| g.title[0].to_s.downcase == society_title[0].downcase}.first
	    #       unless _society
	    #         self.build_society(society_title[0], society_title[1], index+1)
	    #       else
	    #         _society.position = index+1
	    #       end
	    #     end
	    #   end
	    # end

			def populate_galleries
	      if self.galleries.length < 6
	        [
	          'Cover',
	          'Image1',
	          'Image2',
	          'Image3',
	          'Image4',
	          'Image5'
	        ].each_with_index do |media_title, index|
	          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
	          unless _galery
	            self.build_image(media_title, index+1)
	          else
	            _galery.position = index+1
	          end
	        end
	      end if self.new_record?
	    end

	end
end
