module TheProject
	module ProjectSearching
		extend ActiveSupport::Concern

		module ClassMethods
			def by_keywords(_key)
				return if _key.blank?
				query_opts = [
					"LOWER(projects.code) LIKE LOWER(:key)",
					"LOWER(projects.title) LIKE LOWER(:key)",
					"LOWER(users.username) LIKE LOWER(:key)",
					"LOWER(users.email) LIKE LOWER(:key)",
					"LOWER(profiles.full_name) LIKE LOWER(:key)"
				].join(' OR ')
				where(query_opts, {key: "%#{_key}%"} )
			end

			def by_province(_province)
				return if _province.blank?
				where("addresses.province =?", _province)
			end

			def by_city(_city)
				return if _city.blank?
				where("addresses.city =?", _city)
			end

			def by_property(_property)
				return if _property.blank?
				where(type_project: _property)
			end

			def by_construction(_construction)
				return if _construction.blank?
				where(construction: _construction)
			end

			def by_price(_price_min=nil, _price_max=nil)
				return if _price_min.blank? && _price_max.blank?
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?
				if _price_min.blank?
					where("projects.price <= ?", _price_max)
				elsif _price_max.blank?
					where("projects.price >= ?", _price_min)
				else
					where("projects.price <= ?", _price_max)
					.where("projects.price >= ?", _price_min)
				end
			end

			def by_price_meter(_price_min=nil, _price_max=nil)
				return if _price_min.blank? && _price_max.blank?
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?

				if _price_min.blank?
					where("price_in_meter <= ?", _price_max)
				elsif _price_max.blank?
					where("price_in_meter >= ?", _price_min)
				else
					where("price_in_meter <= ?", _price_max)
					.where("price_in_meter >= ?", _price_min)
				end
			end

			def by_project_size(_project_min=nil, _project_max=nil)
				return if _project_min.blank? && _project_max.blank?
				if _project_min.blank?
					where("projects.project_size <= ?", _project_max)
				elsif _project_max.blank?
					where("projects.project_size >= ?", _project_min)
				else
					where("projects.project_size <= ?", _project_max)
					.where("projects.project_size >= ?", _project_min)
				end
			end

			def by_possession_start(_start=nil, _end=nil)
				return if _start.blank? && _end.blank?
				if _start.blank?
					where("DATE(projects.possession_start) <= DATE(?)", _end)
				elsif _end.blank?
					where("projects.possession_start >= DATE(?)", _start)
				else
					where("projects.possession_start <= DATE(?)", _end)
					.where("projects.possession_start >= DATE(?)", _start)
				end
			end

			def by_facillities(_facillities)
				return if _facillities.blank?
				where("facilities @> ARRAY[?]::varchar[]", _facillities)
			end

			def by_search_nav(terms)
				term = terms.split(',').first
				term2 = terms.split(',').last.strip
				queries = [
	      'addresses.city LIKE (:term)',
	      'addresses.sub_district LIKE (:term)',
	      'addresses.village LIKE (:term)',
	      'addresses.area LIKE (:term)'
		    ].join(' OR ')
		    if term2.present?
			    queries2 = ['addresses.city LIKE (:term2)', 'addresses.province LIKE (:term2)'].join(' OR ')
			  end
		    if term2.present?
			    joins(:address).where([queries +" AND "+ queries2, {term: "#{term}", term2: "#{term2}"}])
			  else
			    joins(:address).where([queries, {term: "#{term}"}])
			  end
			end

			def sort_by(_sort)
				return if _sort.blank?
				if _sort == "Newest"
					order(created_at: :desc)
				elsif _sort == "Oldest"
					order(created_at: :asc)
				elsif _sort == "Price (Low to High)"
					order(price: :asc)
				elsif _sort == "Price (High to Low)"
					order(price: :desc)
				else
					return order(id: :desc)
				end
			end

			def search_by(options={})
				results = bonds

				if options[:key].present?
					results = results.by_keywords(options[:key])
				end

				if options[:province].present?
					results = results.by_province(options[:province])
				end

				if options[:city].present?
					results = results.by_city(options[:city])
				end

				if options[:type_project].present?
					results = results.by_property(options[:type_project])
				end

				if options[:construction].present?
					results = results.by_construction(options[:construction])
				end

				if options[:min_price].present? || options[:max_price].present?
					results = results.by_price(options[:min_price], options[:max_price])
				end

				if options[:min_price_meter].present? || options[:max_price_meter].present?
					results = results.by_price_meter(options[:min_price_meter], options[:max_price_meter])
				end

				if options[:min_project_size].present? || options[:max_project_size].present?
					results = results.by_project_size(options[:min_project_size], options[:max_project_size])
				end

				if options[:min_possession_start].present? || options[:max_possession_start].present?
					results = results.by_possession_start(options[:min_possession_start], options[:max_possession_start])
				end

				if options[:facilities].present?
					results = results.by_facillities(options[:facilities])
				end

				if options[:sort].present?
					results = results.sort_by(options[:sort])
				end

				if options[:region_display_name].present?
					results = results.by_search_nav(options[:region_display_name])
				end

				return results
			end
		end

	end
end
