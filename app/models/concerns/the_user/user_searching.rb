module TheUser
	module UserSearching
		extend ActiveSupport::Concern

		module ClassMethods
			def by_keywords(_key)
				return if _key.blank?
				query_opts = [
					"LOWER(users.code) LIKE LOWER(:key)",
					"LOWER(users.username) LIKE LOWER(:key)",
					"LOWER(users.email) LIKE LOWER(:key)",
					"LOWER(profiles.full_name) LIKE LOWER(:key)"
				].join(' OR ')
				where(query_opts, {key: "%#{_key}%"} )
			end

			def search_by(options={})
				results = bonds

				if options[:key].present?
					results = results.by_keywords(options[:key])
				end

				return results
			end

			def find_same(property)
				Property.joins(:address).activated.by_type_property(property.type_property).by_village_id(property.address.village_id).pluck(:id)
			end

			def get_users_same_with_catalog_and_wishilist(property)
				properties_ids = find_same(property)
				users_ids = Wishlist.where(property_id: properties_ids).pluck(:user_id) if properties_ids.present?
				users = User.where(id: users_ids) if users_ids.present?
				users = [] unless users_ids.present?
				return users
			end

		end

	end
end
