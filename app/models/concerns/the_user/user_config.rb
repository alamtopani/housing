module TheUser
	module UserConfig
		extend ActiveSupport::Concern

		included do
			devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, #:confirmable,
	         :validatable, :lockable, :timeoutable, :omniauthable, omniauth_providers: [:facebook], :authentication_keys => [:login]


	    has_one :profile, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :profile, reject_if: :all_blank, allow_destroy: true

	    has_one :address, as: :addressable, dependent: :destroy
	    accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true

	    has_one :contact, as: :contactable, dependent: :destroy
	    accepts_nested_attributes_for :contact, reject_if: :all_blank, allow_destroy: true

	    has_many :properties, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :properties, reject_if: :all_blank, allow_destroy: true

	    has_many :projects, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :projects, reject_if: :all_blank, allow_destroy: true

	    has_many :wishlists, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :wishlists, reject_if: :all_blank, allow_destroy: true

	    has_many :inquiries, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :inquiries, reject_if: :all_blank, allow_destroy: true

	    has_many :my_inquiries, foreign_key: 'owner_id', class_name: 'Inquiry', dependent: :destroy
	    accepts_nested_attributes_for :my_inquiries, reject_if: :all_blank, allow_destroy: true

	    has_many :bid_auctions, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :bid_auctions, reject_if: :all_blank, allow_destroy: true

	    has_many :locations, foreign_key: 'user_id'

	    has_many :testimonials, foreign_key: 'user_id'

	    has_many :sold_reports, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :sold_reports, reject_if: :all_blank, allow_destroy: true

	    has_many :user_packages, dependent: :destroy
	    has_many :packages, through: :user_packages
	    has_one :current_package, class_name: 'CurrentPackageUser'

	    validates_uniqueness_of :username, :email
			validates :email, presence: true

			after_initialize :after_initialized, :default_type
			before_save :downcase_username, :prepare_code
		  after_create :set_free_package

			scope :alfa, ->{order(username: :asc)}
			scope :bonds, ->{eager_load(:profile)}
			scope :not_developer, ->{where.not(type: 'Developer')}
		end

		def is_admin?
			self.type == Admin.name rescue nil
	  end

	  def member?
	    self.type == Member.name rescue nil
	  end

	  def agent?
	    self.type == Agent.name rescue nil
	  end

	  def agent_verify?
	    self.type == Agent.name && self.verified == true
	  end

	  def agent_not_verify?
	    self.type == Agent.name && self.verified == false
	  end

	  def developer?
	    self.type == Developer.name rescue nil
	  end

	  def verified?
	  	if self.verified == true
	  		return "<i class='fa fa-check'></i> Verified".html_safe
		  else
		  	return "<i class='fa fa-warning'></i> Not Verified".html_safe
		  end
	  end

	  def agent_processed?
			if self.agent? && self.verified == false
				return '<div class="btn btn-outlined btn-purple-light btn-sm"><i class="fa fa-warning"></i> Already Processed To Become Agent</div>'.html_safe
			elsif self.agent? && self.verified == true
				return '<div class="btn btn-outlined btn-purple-light btn-sm"><i class="fa fa-check"></i> Already Verify Become Agent</div>'.html_safe
			elsif self.member?
				return '<div class="btn btn-outlined btn-red-light btn-sm"><i class="fa fa-user-secret"></i> Upgrade To Become An Agent</div>'.html_safe
			end
		end

		def fullname
			profile.full_name.present? ? profile.full_name : email
		end

    def set_free_package
    	if self.type == 'Member' || self.type == 'Agent'
		    package = Package.where(name: 'Free Package').first
		    if package
		      up = UserPackage.new({ user_id: self.id, package_id: package.id, confirmation_at: Time.now, payed: true })
		      if up.save
		        up.add_to_current_package
		      end
		    end
		  end
	  end

		private
			def default_type
				self.type = Member.name if self.type.blank?
			end

			def prepare_code
				self.code = SecureRandom.hex(3) if self.code.blank?
			end

			def downcase_username
	      self.username = self.username.downcase if self.username_changed?
	    end

	    def after_initialized
	    	self.profile = Profile.new if self.profile.blank?
	    	self.address = Address.new if self.address.blank?
	    	self.contact = Contact.new if self.contact.blank?
	    end

	end
end
