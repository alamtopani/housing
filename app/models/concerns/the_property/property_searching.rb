module TheProperty
	module PropertySearching
		extend ActiveSupport::Concern

		module ClassMethods
			def by_keywords(_key)
				return if _key.blank?
				query_opts = [
					"LOWER(properties.code) LIKE LOWER(:key)",
					"LOWER(properties.title) LIKE LOWER(:key)",
					"LOWER(users.username) LIKE LOWER(:key)",
					"LOWER(users.email) LIKE LOWER(:key)",
					"LOWER(profiles.full_name) LIKE LOWER(:key)"
				].join(' OR ')
				where(query_opts, {key: "%#{_key}%"} )
			end

			def by_province(_province)
				return if _province.blank?
				where("addresses.province = ?", _province)
			end

			def by_village_id(_village_id)
				return if _village_id.blank?
				where("addresses.village_id = ?", _village_id)
			end

			def by_city(_city)
				return if _city.blank?
				where("addresses.city = ?", _city)
			end

			def by_age(_age_min=nil, _age_max=nil)
				return if _age_min.blank? && _age_max.blank?
				if _age_min.blank?
						where("NULLIF(basic_properties.age, '')::int <= ?", _age_max)
				elsif _age_max.blank?
					where("NULLIF(basic_properties.age, '')::int >= ?", _age_min)
				else
					where("NULLIF(basic_properties.age, '')::int <= ?", _age_max)
					.where("NULLIF(basic_properties.age, '')::int >= ?", _age_min)
				end
			end

			def by_surface_area(_surface_min=nil, _surface_max=nil)
				return if _surface_min.blank? && _surface_max.blank?
				# data = joins(:basic_property)
				if _surface_min.blank?
					where("basic_properties.surface_area <= ?", _surface_max)
				elsif _surface_max.blank?
					where("basic_properties.surface_area >= ?", _surface_min)
				else
					where("basic_properties.surface_area <= ?", _surface_max)
					.where("basic_properties.surface_area >= ?", _surface_min)
				end
			end

			def by_building_area(_building_min=nil, _building_max=nil)
				return if _building_min.blank? && _building_max.blank?
				# data = joins(:basic_property)
				if _building_min.blank?
					where("basic_properties.building_area <= ?", _building_max)
				elsif _building_max.blank?
					where("basic_properties.building_area >= ?", _building_min)
				else
					where("basic_properties.building_area <= ?", _building_max)
					.where("basic_properties.building_area >= ?", _building_min)
				end
			end

			def by_built_up_area(_building_min=nil, _building_max=nil, _type=nil)
				return if _building_min.blank? && _building_max.blank?
				return by_built_up_area_building(_building_min, _building_max) if _type.blank?

				# data = joins(:basic_property)
				if _type == 'Office' || _type == 'Apartment'
					if _building_min.blank?
						where("NULLIF(basic_properties.built_up_area, '')::int <= ?", _building_max)
					elsif _building_max.blank?
						where("NULLIF(basic_properties.built_up_area, '')::int >= ?", _building_min)
					else
						where("NULLIF(basic_properties.built_up_area, '')::int <= ?", _building_max)
						.where("NULLIF(basic_properties.built_up_area, '')::int >= ?", _building_min)
					end
				else
					by_building_area(_building_min, _building_max)
				end
			end

			def by_built_up_area_building(_building_min=nil, _building_max=nil)
				return if _building_min.blank? && _building_max.blank?
				# data = joins(:basic_property)
				if _building_min.blank?
					where("basic_properties.building_area <= :term OR NULLIF(basic_properties.built_up_area, '')::int <= :term", {term: _building_max})
					.where("basic_properties.building_area IS NOT NULL AND basic_properties.built_up_area IS NOT NULL")
				elsif _building_max.blank?
					where("basic_properties.building_area >= :term2 OR NULLIF(basic_properties.built_up_area, '')::int >= :term2", {term2: _building_min})
					.where("basic_properties.building_area IS NOT NULL AND basic_properties.built_up_area IS NOT NULL")
				else
					queries = [
							      'basic_properties.building_area <= :term',
							      'basic_properties.building_area >= :term2'
								    ].join(' AND ')
					queries2 = [
							      "NULLIF(basic_properties.built_up_area, '')::int <= :term",
							      "NULLIF(basic_properties.built_up_area, '')::int >= :term2"
								    ].join(' AND ')

					where([queries +" OR "+ queries2, {term: "#{_building_max}", term2: "#{_building_min}"}])
				end
			end

			def by_price(_price_min=nil, _price_max=nil)
				return if _price_min.blank? && _price_max.blank?
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?

				if _price_min.blank?
					where("properties.price <= ?", _price_max)
				elsif _price_max.blank?
					where("properties.price >= ?", _price_min)
				else
					where("properties.price <= ?", _price_max)
					.where("properties.price >= ?", _price_min)
				end
			end

			def by_price_meter(_price_min=nil, _price_max=nil)
				return if _price_min.blank? && _price_max.blank?
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?

				if _price_min.blank?
					where("basic_properties.price_in_meter <= ?", _price_max)
				elsif _price_max.blank?
					where("basic_properties.price_in_meter >= ?", _price_min)
				else
					where("basic_properties.price_in_meter <= ?", _price_max)
					.where("basic_properties.price_in_meter >= ?", _price_min)
				end
			end

			def by_all_price_type(_price_min=nil, _price_max=nil)
				return if _price_min.blank? && _price_max.blank?
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?
				if _price_min.blank?
					where("basic_properties.price_year <= :term OR properties.price <= :term OR basic_properties.price_month <= :term", {term: _price_max} )
				elsif _price_max.blank?
					where("basic_properties.price_year >= :term2 OR properties.price <= :term2 OR basic_properties.price_month >= :term2", {term2: _price_min} )
				else
					where("basic_properties.price_year <= :term OR properties.price <= :term OR basic_properties.price_month <= :term", {term: _price_max} )
					.where("basic_properties.price_year >= :term2 OR properties.price >= :term2 OR basic_properties.price_month >= :term2", {term2: _price_min} )
				end
			end

			def by_price_type(_price_min=nil, _price_max=nil, _type=nil)
				return if _price_min.blank? && _price_max.blank?
				return by_all_price_type(_price_min, _price_max) if _type.blank?
				# data = joins(:basic_property)
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?

				price_type = (_type == 'Land') ? "basic_properties.price_year" : "basic_properties.price_month"
				if _price_min.blank?
					col = where("#{price_type} <= ?", _price_max)
				elsif _price_max.blank?
					col = where("#{price_type} >= ?", _price_min)
				else
					col = where("#{price_type} <= ?", _price_max)
					.where("#{price_type}  >= ?", _price_min)
				end
				if _type.blank?
					col = where("properties.type_property != 'Land'")
				else
					col = where("properties.type_property = ?", _type)
				end
				col
			end

			def by_price_auction(_price_min=nil, _price_max=nil)
				return if _price_min.blank? && _price_max.blank?
				_price_min = _price_min.split('.').join('') if _price_min.present?
				_price_max = _price_max.split('.').join('') if _price_max.present?
				if _price_min.blank?
					where("auctions.starting_bid <= ?", _price_max)
				elsif _price_max.blank?
					where("auctions.starting_bid >= ?", _price_min)
				else
					where("auctions.starting_bid <= ?", _price_max)
					.where("auctions.starting_bid >= ?", _price_min)
				end
			end

			def by_floor(_floor)
				return if _floor.blank?
				where("basic_properties.floor = ?", _floor)
			end

			def by_bathrooms(_bathrooms)
				return if _bathrooms.blank?
				where("basic_properties.bathrooms = ?", _bathrooms)
			end

			def by_bedrooms(_bedrooms)
				return if _bedrooms.blank?
				where("basic_properties.bedrooms = ?", _bedrooms)
			end

			def by_type_property(_type)
				return if _type.blank?
				where("type_property = ?", _type)
			end

			def by_facillities(_facillities)
				return if _facillities.blank?
				where("facilities @> ARRAY[?]::varchar[]", _facillities)
			end

			def by_category(_category=nil, _type=nil)
				return if _category.blank?
				if _type.blank?
					where("properties.category = ?", _category)
				else
					where("properties.category = ?", _category)
					.where("properties.type_property = ?", _type)
				end
			end

			def by_search_nav(terms)
				term = terms.split(',').first
				term2 = terms.split(',').last.strip
				queries = [
	      'addresses.city LIKE (:term)',
	      'addresses.sub_district LIKE (:term)',
	      'addresses.village LIKE (:term)',
	      'addresses.area LIKE (:term)'
		    ].join(' OR ')
		    if term2.present?
			    queries2 = ['addresses.city LIKE (:term2)', 'addresses.province LIKE (:term2)'].join(' OR ')
			  end
		    if term2.present?
			    joins(:address).where([queries +" AND "+ queries2, {term: "#{term}", term2: "#{term2}"}])
			  else
			    joins(:address).where([queries, {term: "#{term}"}])
			  end
			end

			def by_auction_search(terms)
				term = terms.split(',').first
				term2 = terms.split(',').last.strip
				queries = [
	      'addresses.city LIKE (:term)',
	      'addresses.sub_district LIKE (:term)',
	      'addresses.village LIKE (:term)',
	      'addresses.postcode LIKE (:term)',
	      'addresses.address LIKE (:term)',
	      'properties.code LIKE (:term)',
	      'properties.title LIKE (:term)',
		    ].join(' OR ')
		    if term2.present?
			    queries2 = [
		      'addresses.province LIKE (:term2)',
		      'addresses.city LIKE (:term2)',
		      'addresses.sub_district LIKE (:term2)',
		      'addresses.village LIKE (:term2)',
			    ].join(' OR ')
			  end
		    if term2.present?
			    joins(:address, :auction).activated_auction.where([queries +" AND "+ queries2, {term: "#{term}", term2: "#{term2}"}])
			  else
			    joins(:address, :auction).activated_auction.where([queries, {term: "#{term}"}])
			  end
			end

			def sort_by(_sort)
				return if _sort.blank?
				if _sort == "Newest"
					order(created_at: :desc)
				elsif _sort == "Oldest"
					order(created_at: :asc)
				elsif _sort == "Price (Low to High)"
					order(price: :asc)
				elsif _sort == "Price (High to Low)"
					order(price: :desc)
				else
					return
				end
			end

			def sort_by_auction(_sort)
				return if _sort.blank?
				if _sort == "Newest"
					order(created_at: :desc)
				elsif _sort == "Oldest"
					order(created_at: :asc)
				elsif _sort == "Price (Low to High)"
					order("auctions.starting_bid ASC")
				elsif _sort == "Price (High to Low)"
					order("auctions.starting_bid DESC")
				else
					return
				end
			end

			def sort_by_rent(_sort)
				return if _sort.blank?
				if _sort == "Newest"
					order(created_at: :desc)
				elsif _sort == "Oldest"
					order(created_at: :asc)
				elsif _sort == "Price (Low to High)"
					order("basic_properties.price_month ASC")
				elsif _sort == "Price (High to Low)"
					order("basic_properties.price_month DESC")
				else
					return
				end
			end

			def search_by(options={})
				if options[:bonds] == 'export'
					results = eager_load( {user: [:profile]}, :address, :basic_property)
				else
					results = bonds
				end
				
				if options[:key].present?
					results = results.by_keywords(options[:key])
				end

				if options[:province].present?
					results = results.by_province(options[:province])
				end

				if options[:city].present?
					results = results.by_city(options[:city])
				end

				if options[:min_surface_area].present? || options[:max_surface_area].present?
					results = results.by_surface_area(options[:min_surface_area], options[:max_surface_area])
				end

				# if options[:min_built_up_area].present? || options[:max_built_up_area].present?
				# 	results = results.by_built_up_area(options[:min_built_up_area], options[:max_built_up_area], options[:type_property])
				# end

				if options[:min_building_area].present? || options[:max_building_area].present?
					results = results.by_building_area(options[:min_building_area], options[:max_building_area])
				end

				if options[:floor].present?
					results = results.by_floor(options[:floor])
				end

				if options[:bathrooms].present?
					results = results.by_bathrooms(options[:bathrooms])
				end

				if options[:bedrooms].present?
					results = results.by_bedrooms(options[:bedrooms])
				end

				if options[:type_property].present? && options[:category].blank?
					results = results.by_type_property(options[:type_property])
				end

				if options[:category].present?
					results = results.by_category(options[:category], options[:type_property])
					if options[:min_price].present? || options[:max_price].present?
						if options[:category] == "Rent"
							results = results.by_price_type(options[:min_price], options[:max_price], options[:type_property])
						elsif options[:category] == "Auction Property"
							results = results.by_price_auction(options[:min_price], options[:max_price])
						else
							results = results.by_price(options[:min_price], options[:max_price])
						end
					end
					if options[:sort].present?
						if options[:category] == "Rent"
							results = results.sort_by_rent(options[:sort])
						elsif options[:category] == "Auction Property"
							results = results.sort_by_auction(options[:sort])
						else
							results = results.sort_by(options[:sort])
						end
					end
				else
					if options[:min_price].present? || options[:max_price].present?
						results = results.by_price(options[:min_price], options[:max_price])
						results = results.sort_by(options[:sort]) if options[:sort].present?
					end
				end

				if options[:facilities].present?
					results = results.by_facillities(options[:facilities])
				end

				if options[:min_year].present? || options[:max_year].present?
					results = results.by_age(options[:min_year], options[:max_year])
				end

				if options[:min_price_meter].present? || options[:max_price_meter].present?
					results = results.by_price_meter(options[:min_price_meter], options[:max_price_meter])
				end

				if options[:region_display_name].present?
					results = results.by_search_nav(options[:region_display_name])
				end

				if options[:region_auction_display_name].present?
					results = results.by_auction_search(options[:region_auction_display_name])
				end

				return results
			end

			def top_listing(lim=4)
				user_id_top_listing_package = UserPackage.top_listing.pluck(:user_id)
				where(user_id: user_id_top_listing_package).order("RANDOM()").limit(lim)
			end

		end

	end
end
