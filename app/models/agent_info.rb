class AgentInfo < ActiveRecord::Base
  belongs_to :member, foreign_key: 'user_id'
  belongs_to :agent, foreign_key: 'user_id'

  has_attached_file :company_logo, styles: {
                      large:    '512x512>',
                      medium:   '256x256>',
                      small:    '128x128>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :company_logo, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
