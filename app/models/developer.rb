class Developer < User
  ratyrate_rateable "happiness"
  ratyrate_rater

	extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

	default_scope {where(type: Developer.name)}
end
