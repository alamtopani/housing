class Gender < EnumerateIt::Base
  associate_values :Male, :Female
end
