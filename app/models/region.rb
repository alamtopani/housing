class Region < ActiveRecord::Base
	include ScopeBased
	include Tree
	scope :featured, ->{where(featured: true)}
	scope :alfa, ->{order(name: :asc)}

  has_attached_file :image, styles: {
                      large:    '512x512>',
                      medium:   '256x256>',
                      small:    '128x128>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  def display_name
    return "#{name}, #{parent_city_name}" if parent.present?
    return "#{children_name}, #{name}" if children.present?
  end

  def parent_name
    return unless parent
    "#{parent.name}"
  end

  def parent_city_name
    return unless ancestors
    if ancestors.at_depth(1).present?
      "#{ancestors.at_depth(1).first.name}"
    elsif ancestors
      "#{ancestors.first.name}"
    else
      ""
    end
  end

  def children_name
    return unless children
    "#{children.name}"
  end
end
