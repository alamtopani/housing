class Property < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

	include ScopeBased
  include TheProperty::PropertyConfig
  include TheProperty::PropertySearching

  validates_uniqueness_of :code, if: 'self.code.present?'

  PRICE_LIST=[50000000, 100000000, 200000000, 500000000, 1000000000, 2000000000, 5000000000, 10000000000, 20000000000]
  RENTPRICE_LIST=[1000000, 3000000, 5000000, 10000000, 25000000, 50000000, 100000000, 250000000, 500000000]
  PRICEMETER_LIST=[100000, 250000, 500000, 750000, 1000000, 1500000, 2000000, 5000000, 7500000]
  AREA_LIST=[50, 100, 200, 500, 1000]

  def latitude
    address.latitude
  end

  def longitude
    address.longitude
  end

  def detail_property
    detail_property =  if self.type_property == 'Apartment'
      "Area: <b>#{basic_property.try(:building_area) || '--'} m<sup>2</sup></b> &nbsp; <br> Bathrooms: <b>#{basic_property.try(:bathrooms) || '--'} </b> &nbsp; Bedrooms: <b> #{basic_property.try(:bedrooms) || '--'} </b>"
    elsif self.type_property == 'Office'
      "Area: <b>#{basic_property.try(:building_area) || '--'} m<sup>2</sup></b>"
    elsif self.type_property == 'Land'
      "Land Area: <b>#{basic_property.try(:surface_area) || '--'} m<sup>2</sup></b>"
    elsif self.type_property == 'House'
      "Land Area: <b>#{basic_property.try(:surface_area) || '--'} m<sup>2</sup></b> &nbsp; Building Area: <b>#{basic_property.try(:building_area) || '--'} m<sup>2</sup></b> <br> Bathrooms: <b>#{basic_property.try(:bathrooms) || '--'} </b> &nbsp; Bedrooms: <b>#{basic_property.try(:bedrooms) || '--'} </b>"
    elsif self.type_property == 'Ruko'
      "Land Area: <b>#{basic_property.try(:surface_area) || '--'} m<sup>2</sup></b> &nbsp; Building Area: <b>#{basic_property.try(:building_area) || '--'} m<sup>2</sup></b> <br> Bathrooms: <b>#{basic_property.try(:bathrooms) || '--'} </b>"
    else
      "Land Area: <b>#{basic_property.try(:surface_area) || '--'} m<sup>2</sup></b> &nbsp; Building Area: <b>#{basic_property.try(:building_area) || '--'} m<sup>2</sup></b>"
    end

    detail_property.html_safe
  end

  def price_for_meter
    if self.category == 'Sell' && (self.type_property == 'Apartment' || self.type_property == 'Office' || self.type_property == 'House')
      self.price_per_meter
    else
      ""
    end
  end
end