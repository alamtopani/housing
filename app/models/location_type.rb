class LocationType < EnumerateIt::Base
  associate_values(
    :airports             	=> ['Airports', 'Airports'],
    :train_stations         => ['Train Stations', 'Train Stations'],
    :bus_stations						=> ['Bus Stations', 'Bus Stations'],
    :bank										=> ['Banks', 'Banks'],
    :hospitals							=> ['Hospitals', 'Hospitals'],
    :schools								=> ['Schools', 'Schools'],
    :parks									=> ['Parks', 'Parks'],
    :restaurants						=> ['Restaurants', 'Restaurants'],
    :movie_theaters					=> ['Movie Theaters', 'Movie Theaters'],
    :bars										=> ['Bars', 'Bars'],
    :department_stores			=> ['Department Stores', 'Department Stores'],
    :pharmacies							=> ['Pharmacies', 'Pharmacies'],
    :shopping_malls					=> ['Shopping Malls', 'Shopping Malls']
  )
  sort_by :value
end
