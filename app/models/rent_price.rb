class RentPrice < ActiveRecord::Base
	belongs_to :rent_priceable, polymorphic: true

	default_scope { order('rent_prices.position ASC') }
	include ScopeBased
end
