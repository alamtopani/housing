class Project < ActiveRecord::Base
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  include ScopeBased
  include TheProject::ProjectConfig
  include TheProject::ProjectSearching

  validates_uniqueness_of :code, if: 'self.code.present?'

  PRICE_LIST=[50000000, 100000000, 200000000, 500000000, 1000000000, 2000000000, 5000000000, 10000000000, 20000000000]
  PRICEMETER_LIST=[100000, 250000, 500000, 750000, 1000000, 1500000, 2000000, 5000000, 7500000]

  def status_area?
    if self.type_project == 'Apartment' || self.type_project == 'Office Tower'
      return 'Gross Area in m<sup>2</sup>'.html_safe
    else
      return 'Area m<sup>2</sup>'.html_safe
    end
  end

  def status_price?
    if self.type_project == 'House'
      return 'Starting Price'
    else
      return ''
    end
  end

  def latitude
    address.latitude
  end

  def longitude
    address.longitude
  end

  def price_for_meter
    if self.type_project != 'House'
      self.price_in_meter
    else
      ""
    end
  end

end
