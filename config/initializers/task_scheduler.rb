scheduler = Rufus::Scheduler.new

scheduler.cron(ENV['CRON_TASK_TIME']) do
   SendNewPropertyWishlist.new.send_email!
end

# Send the digest every week
# scheduler.cron("0 0 * * 0") do
#    SendNewPropertyWishlist.new.send_email!
# end

# Send the digest every minutes
# scheduler.cron("* * * * *") do
#    SendNewPropertyWishlist.new.send_email!
# end
