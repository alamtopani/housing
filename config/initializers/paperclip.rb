Paperclip.options[:content_type_mappings] = {
  :png => "application/octet-stream",
  :jpg => "application/octet-stream",
  :jpeg => "application/octet-stream"
}

Paperclip::Attachment.default_options[:use_timestamp] = false
