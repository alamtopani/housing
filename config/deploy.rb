# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'housing'
set :repo_url, 'git@gitlab.com:alamtopani/housing.git'
set :scm, :git
set :deploy_to, '/home/housing/www'

set :format, :pretty
set :log_level, :info

set :linked_files, %w{config/database.yml config/local_env.yml}
set :linked_dirs, %w{bin log tmp vendor/bundle public/system}

set :rbenv_path, '/home/housing/.rbenv'

set :default_env, {
   path: "/home/housing/.rbenv/shims:/home/housing/.rbenv/bin:$PATH"
 }

set :keep_releases, 3

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'unicorn:restart'
    end
  end

  after :finishing, 'deploy:cleanup'
end

namespace :sidekiq do
  def stop_sidekiq(pid_file)
    if fetch(:stop_sidekiq_in_background, fetch(:sidekiq_run_in_background))
      if fetch(:sidekiq_use_signals)
        execute "kill -TERM `cat #{pid_file}`"
      else
        execute :bundle, :exec, :sidekiqctl, 'stop', "#{pid_file}", fetch(:sidekiq_timeout)
      end
    else
      execute :bundle, :exec, :sidekiqctl, 'stop', "#{pid_file}", fetch(:sidekiq_timeout)
    end

    if pid_process_exists?(pid_file)
      execute "kill -15 `cat #{pid_file}` || true"
    end
  end
end

after 'deploy:publishing', 'deploy:restart'
