role :app, %w{housing@63.142.252.102}
role :web, %w{housing@63.142.252.102}
role :db,  %w{housing@63.142.252.102}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '63.142.252.102', user: 'housing', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.2.2'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/housing/www"
set :rails_env,       "production"
set :branch,          "staging"

set :sidekiq_concurrency, 20
set :sidekiq_queue, ['default']
set :sidekiq_timeout, 300
set :sidekiq_pid,  File.join(shared_path, 'tmp', 'pids', 'sidekiq.pid')
set :sidekiq_env,  fetch(:rack_env, fetch(:rails_env, fetch(:stage)))
set :sidekiq_log,  File.join(shared_path, 'log', 'sidekiq.log')

set :unicorn_config_path, "/home/housing/www/current/config/unicorn.rb"
