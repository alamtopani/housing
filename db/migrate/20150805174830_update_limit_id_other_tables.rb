class UpdateLimitIdOtherTables < ActiveRecord::Migration
  def change
    change_column :regions, :id, :integer, limit: 8
    change_column :properties, :id, :integer, limit: 8
    change_column :locations, :id, :integer, limit: 8
    change_column :projects, :id, :integer, limit: 8
    change_column :addresses, :id, :integer, limit: 8
  end
end
