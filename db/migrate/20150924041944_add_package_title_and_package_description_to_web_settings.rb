class AddPackageTitleAndPackageDescriptionToWebSettings < ActiveRecord::Migration
  def change
    add_column :web_settings, :package_title, :string
    add_column :web_settings, :package_description, :text
  end
end
