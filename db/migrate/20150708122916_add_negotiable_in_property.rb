class AddNegotiableInProperty < ActiveRecord::Migration
  def change
  	add_column :properties, :negotiable, :boolean
  	add_column :properties, :price_type, :string
  end
end
