class AddLongAndWidthLandInBasicProperty < ActiveRecord::Migration
  def change
  	add_column :basic_properties, :width_land, :string
  	add_column :basic_properties, :long_land, :string
  end
end
