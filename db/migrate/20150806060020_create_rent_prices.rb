class CreateRentPrices < ActiveRecord::Migration
  def change
    create_table :rent_prices do |t|
      t.string :name
      t.decimal :price
      t.string :price_type
      t.string :rent_priceable_type
      t.integer :rent_priceable_id
      t.integer :position

      t.timestamps null: false
    end
  end
end
