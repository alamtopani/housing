class AddColumnLabelTagsToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :label_tags, :string
  end
end
