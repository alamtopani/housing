class AddAmenitiesAndSocietiesInPropertiesAndProjects < ActiveRecord::Migration
  def change
  	add_column :properties, :facilities, :string
  	add_column :projects, :facilities, :string
  end
end
