class AddReserveMetInBidAuctions < ActiveRecord::Migration
  def change
  	add_column :auctions, :reserve_met, :string
  	add_column :auctions, :financing_considered, :string
  end
end
