class CreateReportSolds < ActiveRecord::Migration
  def change
    create_table :sold_reports do |t|
      t.integer :user_id
      t.decimal :price
      t.string :sold_reportable_type
      t.integer :sold_reportable_id
      t.datetime :date

      t.timestamps null: false
    end

    add_index :sold_reports, :user_id
  end
end
