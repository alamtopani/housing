class AddLinkUrlOnLandingPages < ActiveRecord::Migration
  def change
    add_column :landing_pages, :link_url, :text
  end
end
