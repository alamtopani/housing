class AddMetaSeoSecondOnWebSettings < ActiveRecord::Migration
  def change
    add_column :web_settings, :language, :string
    add_column :web_settings, :country, :string
    add_column :web_settings, :content_language, :string
    add_column :web_settings, :distribution, :string
    add_column :web_settings, :generator, :string
    add_column :web_settings, :rating, :string
    add_column :web_settings, :target, :string
    add_column :web_settings, :search_engines, :text
  end
end
