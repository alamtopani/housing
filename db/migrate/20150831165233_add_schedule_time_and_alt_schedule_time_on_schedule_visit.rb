class AddScheduleTimeAndAltScheduleTimeOnScheduleVisit < ActiveRecord::Migration
  def change
    add_column :schedule_visits, :schedule_time, :string
    add_column :schedule_visits, :alt_schedule_time, :string
  end
end
