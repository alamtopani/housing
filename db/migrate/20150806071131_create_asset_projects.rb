class CreateAssetProjects < ActiveRecord::Migration
  def change
    create_table :asset_projects do |t|
      t.integer :project_id
      t.integer :location_id
      t.string :distance
      t.string :duration
      t.string :type_asset

      t.timestamps null: false
    end
  end
end
