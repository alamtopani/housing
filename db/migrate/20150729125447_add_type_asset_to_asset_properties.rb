class AddTypeAssetToAssetProperties < ActiveRecord::Migration
  def change
    add_column :asset_properties, :type_asset, :string
  end
end
