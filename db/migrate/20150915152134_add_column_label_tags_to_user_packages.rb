class AddColumnLabelTagsToUserPackages < ActiveRecord::Migration
  def change
    add_column :user_packages, :label_tags, :string
  end
end
