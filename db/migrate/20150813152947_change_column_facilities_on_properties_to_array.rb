class ChangeColumnFacilitiesOnPropertiesToArray < ActiveRecord::Migration
  def change
    change_column :properties, :facilities, "varchar[] USING (string_to_array(facilities, ','))"
    change_column :projects, :facilities, "varchar[] USING (string_to_array(facilities, ','))"
  end
end
