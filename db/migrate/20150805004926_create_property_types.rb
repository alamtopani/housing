class CreatePropertyTypes < ActiveRecord::Migration
  def change
    create_table :property_types do |t|
      t.string :name
      t.string :ancestry
      t.string :slug

      t.timestamps null: false
    end
  end
end
