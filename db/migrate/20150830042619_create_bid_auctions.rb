class CreateBidAuctions < ActiveRecord::Migration
  def change
    create_table :bid_auctions do |t|
      t.integer :user_id
      t.integer :property_id
      t.float :bid

      t.timestamps null: false
    end

    add_index :bid_auctions, :user_id
    add_index :bid_auctions, :property_id
  end
end
