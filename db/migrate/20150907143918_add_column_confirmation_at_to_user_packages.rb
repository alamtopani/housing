class AddColumnConfirmationAtToUserPackages < ActiveRecord::Migration
  def change
    add_column :user_packages, :confirmation_at, :datetime
  end
end
