class CreateAmenities < ActiveRecord::Migration
  def change
    create_table :amenities do |t|
      t.string :title
      t.boolean :available
      t.string :amenitiable_type
      t.integer :amenitiable_id
      t.string :icon
      t.integer :position

      t.timestamps null: false
    end
  end
end
