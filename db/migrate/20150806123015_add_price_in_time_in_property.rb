class AddPriceInTimeInProperty < ActiveRecord::Migration
  def change
  	add_column :basic_properties, :price_security, :decimal
  	add_column :basic_properties, :price_day, :decimal
  	add_column :basic_properties, :price_month, :decimal
  	add_column :basic_properties, :price_year, :decimal
  end
end
