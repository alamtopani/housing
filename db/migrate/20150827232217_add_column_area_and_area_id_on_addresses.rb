class AddColumnAreaAndAreaIdOnAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :area, :string
    add_column :addresses, :area_id, :integer, default: 0, limit: 8
  end
end
