class CreateAgentInfos < ActiveRecord::Migration
  def change
    create_table :agent_infos do |t|
      t.integer :user_id
      t.string :specialist_property
      t.string :specialist_area
      t.text :description
      t.string :language
      t.string :name_company
      t.attachment :company_logo

      t.timestamps null: false
    end

    add_index :agent_infos, :user_id
  end
end
