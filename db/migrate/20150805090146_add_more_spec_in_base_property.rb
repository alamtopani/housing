class AddMoreSpecInBaseProperty < ActiveRecord::Migration
  def change
  	add_column :basic_properties, :document, :string
    add_column :basic_properties, :surface_area, :string
    add_column :basic_properties, :building_area, :string
    add_column :basic_properties, :electricity, :string
    add_column :basic_properties, :phone_lines, :integer
    add_column :basic_properties, :garage, :integer
    add_column :basic_properties, :price_in_meter, :decimal
    add_column :basic_properties, :business_unit, :string
  end
end
