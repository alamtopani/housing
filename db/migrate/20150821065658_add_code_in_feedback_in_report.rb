class AddCodeInFeedbackInReport < ActiveRecord::Migration
  def change
  	add_column :feedbacks, :code, :string
  	add_column :reports, :code, :string
  end
end
