class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :name
      t.string :email
      t.string :category
      t.text :message
      t.string :phone
      t.boolean :status, default: false

      t.timestamps null: false
    end
  end
end
