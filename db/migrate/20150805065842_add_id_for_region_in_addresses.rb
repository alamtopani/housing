class AddIdForRegionInAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :province_id, :integer
    add_column :addresses, :city_id, :integer
    add_column :addresses, :subdistrict_id, :integer
    add_column :addresses, :village_id, :integer
  end
end
