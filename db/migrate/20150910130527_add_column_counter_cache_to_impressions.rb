class AddColumnCounterCacheToImpressions < ActiveRecord::Migration
  def change
    add_column :impressions, :counter_cache, :integer
  end
end
