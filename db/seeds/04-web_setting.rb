module SeedWebSetting
  def self.seed
    WebSetting.find_or_create_by({
      header_tags: 'PROPEDIA',
      footer_tags: 'PROPEDIA',
      contact: '(0266) 234657',
      email: 'sales@housingindonesia.com',
      facebook: 'http://www.facebook.com/',
      twitter: 'http://www.twitter.com/',
      title: 'PROPEDIA',
      keywords: 'PROPEDIA, Property Indonesia, Situs Jual beli rumah, Marketplace',
      description: 'Situs jual beli rumah, apartemen, tanah, gedung, di Indonesia',
      robot: 'Follow',
      author: 'PT.PROPEDIA',
      corpyright: 'PT PROPEDIA',
      revisit: '2 Days',
      expires: 'Never',
      revisit_after: '2 Days',
      geo_placename: 'Indonesia',
      language: 'ID',
      country: 'ID',
      content_language: 'All-Language',
      distribution: 'global',
      generator: 'website',
      rating: 'general',
      target: 'global',
      search_engines: 'Housing, Property Indonesia, Jual Beli Rumah, Rumah Barum Rumah Bekas, Situs property nomor satu, marketplace, jualan, cara membeli rumah'
    })
  end
end
