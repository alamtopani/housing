module SeedAssetFacility
	ASSETS = [
		['AC', '<i class="fa fa-asterisk"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business"]'],
    ['TV', '<i class="fa fa-desktop"></i>','["House","Apartment","Ruko","Factory","Warehouse"]'],
    ['Sofa', '<i class="fa fa-wheelchair"></i>','["House","Apartment","Warehouse"]'],
    ['Dining Table', '<i class="fa fa-square"></i>','["House","Apartment","Factory","Warehouse"]'],
    ['Microwave', '<i class="fa fa-toggle-on"></i>','["House","Apartment","Warehouse"]'],
    ['Fridge', '<i class="fa fa-columns"></i>','["House","Apartment","Warehouse"]'],
    ['Stove', '<i class="fa fa-fire"></i>','["House","Apartment","Warehouse"]'],
    ['Washing Machine', '<i class="fa fa-refresh"></i>','["House","Apartment","Warehouse"]'],
    ['Servant Room', '<i class="fa fa-child"></i>','["House","Apartment","Ruko","Office","Warehouse","Commercial Business"]'],
    ['Cupboard', '<i class="fa fa-th-list"></i>','["House","Apartment","Office","Warehouse","Commercial Business"]'],
    ['Kitchen','<i class="fa fa-filter"></i>','["House","Apartment","Office","Warehouse"]'],
    ['Hot Water','<i class="fa fa-yelp"></i>','["House","Apartment","Warehouse"]'],
    ['Water Purifier','<i class="fa fa-bookmark"></i>','["House","Apartment","Ruko","Warehouse"]'],
    ['Internet','<i class="fa fa-random"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business"]'],
    ['Wifi','<i class="fa fa-wifi"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business"]'],
    ['Security','<i class="fa fa-shield"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business"]'],

    ['Gym', '<i class="fa fa-chain"></i>','["Apartment","Office","Commercial Business","Condotel","Office Tower"]'],
    ['Swimming Pool', '<i class="fa fa-life-saver"></i>','["House","Apartment","Office","Warehouse","Commercial Business","Condotel","Office Tower"]'],
    ['Gas Pipeline', '<i class="fa fa-sliders"></i>','["House","Apartment","Ruko","Factory","Warehouse","Commercial Business","Condotel","Office"]'],
    ['Power Backup', '<i class="fa fa-lightbulb-o"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business","Condotel","Office Tower"]'],
    ['Water Supply', '<i class="fa fa-tint"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business","Condotel","Office Tower"]'],
    ['Gated Community', '<i class="fa fa-inbox"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business"]'],
    ['Garden', '<i class="fa fa-tree"></i>','["House","Apartment","Office","Warehouse","Commercial Business","Condotel","Office Tower"]'],
    ['Kids Play Area', '<i class="fa fa-smile-o"></i>','["House","Apartment","Warehouse","Condotel","Office"]'],
    ['Sports Facility', '<i class="fa fa-street-view"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business","Condotel","Office Tower"]'],
    ['Lift', '<i class="fa fa-arrows-v"></i>','["Apartment","Office","Commercial Business","Condotel","Office Tower"]'],
    ['Parking','<i class="fa fa-car"></i>','["House","Apartment","Ruko","Factory","Office","Warehouse","Commercial Business","Condotel","Office Tower"]']
	]

	def self.seed
		ASSETS.each do |asset|
			facility = AssetFacility.find_or_initialize_by(name: asset[0])
			facility.font_icon = asset[1]
			facility.category = asset[2]
			facility.save
		end
	end
end
