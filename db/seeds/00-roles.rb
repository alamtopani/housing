module SeedRole
  def self.seed
    Role.create({ name: "admin", title: 'Role for Admin', description: "This User Can Do Anything", the_role: "{\"system\":{\"administrator\":true}}"})
  end
end
