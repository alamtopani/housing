module SeedPropertyType
	TYPES = {
		"Sell"=> [
			"House",
			"Apartment",
			"Ruko",
			"Factory",
			"Land",
			"Office",
			"Warehouse",
			"Commercial Business"
		],

		"Rent"=> [
			"House",
			"Apartment",
			"Ruko",
			"Land",
			"Office",
			"Warehouse"
		],

		"Auction Property"=> [
			"House",
			"Apartment",
			"Ruko"
		],

		"New Project" => [
			"House",
			"Apartment",
			"Office Tower",
			"Condotel"
		]
	}

	def self.seed
		TYPES.keys.each_with_index do |property_root, index|
			category = PropertyType.find_or_initialize_by(name: property_root)
			category.name = property_root
			category.save

			TYPES[category.name].each do |c|
				child = PropertyType.new
				child.parent = category
				child.name = c
				child.save
			end
		end
	end
end
