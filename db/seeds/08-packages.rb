module SeedPackage

  def self.seed
    pack = Package.find_or_initialize_by({name: "Free Package"})
    pack.duration = 0
    pack.description = "Package For New User"
    pack.price = 0
    pack.active = true
    pack.max_listing = 5
    pack.gratis_bulan = 0
    pack.save
    puts "created Free Package"
  end

end
